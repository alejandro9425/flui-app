import styled from 'styled-components';

const Contenedor = styled.div`
    margin: 75px auto;
    width: 75%;
    border: 1px solid #D3D3D3;
    @media (min-width: 596px) {
        width: 60%;
    }

    @media (min-width: 601px) {
            width: 55%;
    }

    @media (min-width: 701px) {
            width: 35%;
    }

    @media (min-width: 1080px) {
            width: 25%;
    }
    
`;

const ContenedorLogin = styled.div`
    background-color: #FFFFFF;
    padding: 20px;
    border-radius: 5px;
`;

const ContenedorLoginHeader = styled.div`
    text-align: center;
    color: #777;
`;

const ContenedorInputs = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-top: 10%;
`;

const GroupInputs = styled.div`
    width: 100%;
    margin-bottom: 10px;
`;

const Input = styled.input`
    text-align: center;
    background-color: #ECF0F1;
    border: 2px solid transparent;
    border-radius: 3px;
    font-size: 16px;
    font-weight: 200;
    padding: 10px 0;
    width: 100%;
    transition: border .5s;

    &:focus{
        border: 2px solid #3498DB;
        box-shadow: none;
    }
`;

const Boton = styled.button`
    border: 2px solid transparent;
    background: #3498DB;
    color: #ffffff;
    font-size: 16px;
    line-height: 25px;
    padding: 10px 0;
    text-align: center;
    text-decoration: none;
    text-shadow: none;
    border-radius: 3px;
    box-shadow: none;
    transition: 0.25s;
    display: block;
    width: 100%;
    margin: 0 auto;
    margin-top: 1%;
    &:hover{
        background: #2980B9;
        cursor: pointer;
    }
`;

const BotonGoogle = styled.button`
    border: 1px solid #DDDDDD;
    background: #FFFFFF;
    color: #000000;
    font-size: 16px;
    line-height: 25px;
    padding: 10px 0;
    text-align: center;
    text-decoration: none;
    text-shadow: none;
    border-radius: 3px;
    box-shadow: none;
    transition: 0.25s;
    display: block;
    width: 100%;
    margin: 0 auto;
    margin-top: 8%;
    display:flex;
    justify-content: space-around;
    &:hover{
        background: #FFFFFF;
        cursor: pointer;
    }
    svg{
        background: #F90101;
        fill: #FFFFFF;
        padding: 3px;
        border-radius: 2px;
    }
`;

const Link = styled.a`
    font-size: 12px;
    color: #444;
    display: block;
    margin-top: 12px;
`;

const GroupLinks = styled.div`
    margin-top: 5px;
    padding: 5px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

const Elementlink = styled.a `
    color: #000000;
    text-decoration: none;
    margin-top:5px;
`;

const AlertAuth = styled.p`
    padding: 10px;
    background: #f8d7da;
    color: red;
    text-align: center;
`

export {AlertAuth,Contenedor, ContenedorLogin, ContenedorLoginHeader,ContenedorInputs,
        GroupInputs,Input, Boton, BotonGoogle, Link, GroupLinks, Elementlink};