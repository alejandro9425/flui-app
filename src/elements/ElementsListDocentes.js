import styled from 'styled-components'


const H1 = styled.h1`
    margin-top: 2%;
    text-align:center;
    color: #FFFFFF !important;
`;


const ContainerDocente = styled.div`
    width: 95%;
    margin: auto;
    margin-top: 5%;
    overflow-x:auto;
`;

const Table = styled.table`
    background: #FFFFFF;
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
`;

const THead = styled.thead`

`;

const TBody = styled.tbody`

`;

const Tr = styled.tr`
    :nth-child(even){background-color: #f2f2f2}
`;

const Th = styled.th`
    text-align: center;
    padding: 8px;
`;

const Td = styled.td`
    text-align: center;
    padding: 8px;
    white-space: nowrap;
`;

const Image = styled.img`
    border-radius: 50%;
    width: 50px;
    height: 50px;
`;

const I = styled.i`

`;

export {H1, ContainerDocente, Table,THead, TBody, Tr, Th, Td, Image, I}