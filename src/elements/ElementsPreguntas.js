import styled from 'styled-components';
const Conteiner = styled.div`
    width: 80%;
    margin:auto;
    margin-bottom: 5%;
`;
const Titulo = styled.h3`
    text-transform: uppercase;
    text-align:center;
`;
const Preguntas = styled.div`
    margin-top: 3%;
    display:flex;
    flex-direction:column;
`;
const AddPregunta = styled.div`
    margin-bottom:5px;
`;
const I = styled.i`
    font-weight:normal;
`;

const InputPregunta = styled.input`
    border: none;
    outline:none;
    border-bottom: 2px solid #515151;
    padding: 5px;
    margin-right: 10px;
`;

const ContPreguntas = styled.div`
    width: 100%;
    display:flex;
    flex-wrap:wrap;
    padding: 2%;
    justify-content:space-around;
`;

const PreguntaContainer = styled.div`
    margin-top:2%;
    width: 30%;
    display:flex;
    flex-direction:column;
    background: #0f1f39;
    -webkit-box-shadow: 3px 4px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 3px 4px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 3px 4px 5px 0px rgba(0,0,0,0.75);
`;

const PreguntaHeader = styled.div`
    width: 85%;
    margin:auto;
    display:flex;
    justify-content:space-between;
    align-items:center;
    padding: 10px;
    color: #FFFFFF;
    > i {
        color: #FFFFFF;
        :hover{
            cursor: pointer;
            color: #FFFFFF;
        }
    }
`;

const PreguntaBody = styled.div`
    width: 85%;
    margin: auto;
    padding: 10px;
    border-top: 2px solid #d6d6d6;
    color: #FFFFFF;

`;

const TextPregunta = styled.b`
    width:70%;
`;

const Text = styled.h4`
    font-size: 15px;
    a {
        > i {
            color: #FFFFFF;
            :hover{
                color: #FFFFFF;
            }
        }
    }
`;

const InputPreguntaEdit = styled.input`
    border: none;
    outline:none;
    width: 70%;
`;

export {
    Conteiner,Titulo,Preguntas,AddPregunta,I,
    InputPregunta,ContPreguntas, PreguntaContainer,
    PreguntaHeader,PreguntaBody,TextPregunta,Text,
    InputPreguntaEdit
}