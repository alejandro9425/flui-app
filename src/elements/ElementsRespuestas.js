import styled from 'styled-components';

const Contenedor = styled.div`
    width: 80%;
    margin:auto;
    margin-top: 5%;
`;
const Ul = styled.ul`
    display:flex;
    justify-content:space-around;
    list-style:none;
`;

const Li = styled.li`
    a{
        text-decoration:none;
    }
`;

const Table = styled.table`
    width: 100%;
    margin:auto;
`;
const Th = styled.th`
    padding:5px;
    text-align:center;
    .calificacion{
       background:red !important;
    }
    
`;

const Tr = styled.tr`

`;

const Td = styled.td`
    padding:5px;
    text-align:center;
    &&.calificacion{
        width: 250px;
    }
`;

const Tbody = styled.tbody`

`;

const Thead = styled.thead`
    border-bottom: 2px solid #D3D3D3;
`;

const I = styled.i``;

const Input = styled.input`
    width: 25%;
    margin-right: 10px;
    border:none;
    outline:none;
`;

export {
    Contenedor,Ul,Li, Table, Th,Thead,Tbody, Tr,Td, I,Input
}