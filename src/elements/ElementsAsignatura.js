import styled from 'styled-components';

const ContainerAddAsignatura = styled.div`
    width: 90%;
    margin:auto;
    margin-top: 3%;
    margin-bottom: 5%;
`;

const Title = styled.h2`
    text-align:center;
    margin-bottom:2%;
`;

const Form = styled.form`
    display: flex;
    flex-direction:column;
    border: 1px solid #D3D3D3;
    padding: 1%;
    margin-bottom: 1%;
`;

const Label = styled.label`
    text-align:center;
    margin-bottom: 2%;
`;

const Input = styled.input`
    border-radius: 5px;
    border: none;
    border-bottom: 1px solid #D4D4D4;
    width: 35%;
    padding: 8px;
    margin: auto;
    margin-bottom: 1%;
    text-align:center;
    :focus{
        outline:none;
    }
`;

const Button = styled.button`
    border-radius: 5px;
    border: none;
    padding: 1%;
    width: 10%;
    margin:auto;
    margin-bottom: 1%;
    background: #198754;
    color: #FFFFFF;
`;

const Table = styled.table`
    
    margin-top: 5%;
    width: 100%;
    
`;

const Thead = styled.thead`
    
`; 
const Tbody = styled.tbody`

`;

const Tr = styled.tr`
    text-align:center;
    border: 3px solid #D3D3D3;
`;

const Th = styled.th`
    border: 3px solid #D3D3D3;
`;

const Td = styled.td`
    border: 3px solid #D3D3D3;
    padding: 5px;
`;

const Inputedit = styled.input`
    outline: none;
    border:none;
    border-bottom: 2px solid #D3D3D3;
    width: 60%;
    text-align:center;
`;

const FormularioAdd = styled.form`

`;

const I = styled.i`

`;

const ContainerCard = styled.div`
    margin-top: 5%;
    display:flex;
    flex-wrap:wrap;
    justify-content: space-around;
`;

const Card = styled.div`
    background:#F1F1F1;
    width: 35%;
    display:flex;
    margin-top: 2%;
    -webkit-box-shadow: 3px 0px 5px 1px rgba(148,145,148,1);
    -moz-box-shadow: 3px 0px 5px 1px rgba(148,145,148,1);
    box-shadow: 3px 0px 5px 1px rgba(148,145,148,1);

`;

const CardSeccion = styled.div`
    width:60%;
    padding: 2%;
`;

const CardSeccionD = styled.div`
    width:40%;
    padding: 2%;
    display:flex;
    
    justify-content:center;
    flex-direction:column;
    > div {
        width: 100%;
        display:flex;
        justify-content:center;
    }
`;

const CardTitle = styled.h3`
    text-align:center;
`; 

const SeccionCard = styled.div`
    display:flex;
    margin-bottom: 2%;
    
`; 

const TextSeccion = styled.div`
    width: 45%;
    display:flex;
    text-align:center;
    flex-direction:column;
    > b {
        text-align:center;
    }
    > i {
        text-align:center;
    }
`; 

const Separador = styled.div`
    display:flex;
    align-items:center;
    width: 10%;
    > b {
        width: 100%;
        text-align:center;
    }
`;

const CardFooter = styled.div`

    display:flex;
    justify-content:space-between;
    > i{
        width: 45%;
        text-align:center;
    }
`;

const CardView = styled.div`
    margin-top: 1%;
    display:flex;
    justify-content:center;
    > i{
        width: 45%;
        text-align:center;
    }
`;

const Imagen = styled.img`
    width: 65%;
    height: 100px;
    margin:auto;    
`;

export {
    ContainerAddAsignatura,Title,Form,Label,Input,Button,Table,
    Tr,Th,Td,Thead,Tbody, Inputedit,FormularioAdd,I,
    ContainerCard, Card,CardSeccion,CardTitle,SeccionCard,
    TextSeccion, Separador,CardFooter, Imagen,CardSeccionD,CardView
}