import styled from 'styled-components';

const Ul = styled.ul`
    margin:auto;
    margin-top:5%;
    display:flex;
    width:75%;
    justify-content:space-around;
    border-bottom: 1px solid #09124f;
`;

const Li = styled.ul`
     
        padding:7px;
        > a{
            padding:10px;
            text-decoration:none;
        }
        >.active{
            padding:10px;
            color:#FFFFFF;
            background:#09124f;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;        
        }
        i{
            padding: 5px;
            :hover{
                cursor: pointer;
            }
        }
`;
const Element = styled.div``;
const ContProgress = styled.div`
    width:80%;
    margin:auto;
    a {
        text-decoration:none;
        font-weight:bold;
        color:#0088cc;
            :hover{
                color:#0088cc;
            }
    }
`;
const Table = styled.table`
    width: 80%;
    margin:auto;
    margin-top:2%;
`;
const THead = styled.thead`
    width:100%;
`;
const TBody = styled.tbody`
    width:100%;
`;
const Tr = styled.tr`
    
`;
const Th = styled.th`
    border:2px solid #D3D3D3;
    text-align:center;
`;
const Td = styled.td`
    text-align:center;
    padding: 5px;
    border-bottom: 2px solid #D3D3D3;
    a {
        text-decoration:none;
        font-weight:bold;
    }
`;

const I = styled.i`

`;

const Input = styled.input`
    width: 65%;
    border:none;
    outline:none;
    border-bottom: 1px solid #000000;
    padding: 5px;
    text-align:center;
`;

export {
    Ul, Li, Element,
    Table,THead,TBody,Tr,Th,Td,I,Input,ContProgress
}