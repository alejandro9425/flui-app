import styled from 'styled-components';

const Container = styled.div`
    width: 90%;
    margin:auto;
    margin-top: 3%;
    margin-bottom: 5%;
    display:flex;
    flex-direction:column;
`;

const Article = styled.article`
    width: 85%;
    margin:auto;
    text-align:justify;
`;

const Hr = styled.hr`

`;

const Img = styled.img`
    width: 50%;
    height: 200px;
    display:block;
    margin:auto;
`;

const Img2 = styled.img`
    width: 50%;
    height: 250px;
    display:block;
    margin:auto;
`;

export {
    Container, Article, Hr,Img, Img2
}