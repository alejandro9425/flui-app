import styled from 'styled-components';

const ControlsButtons = styled.div`
    width:80%;
    margin:auto;
    margin-bottom: 1%;
    display:flex;
    justify-content:end;
    button{
        background: #0f1f39;
        color: #FFFFFF;
        border:none;
        outline:none;
    }
`;
const Contenedor = styled.div`
    width: 80%;
    margin:auto;
    div{
        display:flex;
        justify-content:end;
    }
`;
const TextArea = styled.textarea`
    margin-top: 2%;
    text-align:justify;
    width: 100%;
    border: 1px solid #D3D3D3;
    min-height: 100px;
    max-height: 100px;
    outline:none;

`;

const Div = styled.div`
    display:flex;
    justify-content:end;
    margin-top: 1%;
`;

const I = styled.i`
    padding: 1%;
`;

export {
    ControlsButtons, TextArea,Contenedor,I,Div
}