import styled from 'styled-components';

const Container = styled.div`
    width: 90%;
    margin:auto;
    margin-top: 3%;
    margin-bottom: 5%;
`;

const Img = styled.img`
    width:70%;
    display:block;
    margin:auto;
`;

export {
    Container, Img
}