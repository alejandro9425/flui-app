import styled from 'styled-components';


const ContainerInactivate = styled.div`
    width: 80%;
    margin:auto;
    margin-top:3%;
    display:flex;
    flex-direction:column;
`;

const Image = styled.img`
    width:70%;
    margin:auto;
`;

const H3 = styled.h3`
    width: 80%;
    margin:auto;
    margin-top: 1%;
    color: #000000;
    text-align:center;
`;

const H5 = styled.h5`
    width: 80%;
    margin:auto;
    margin-top: 1%;
    color: #000000;
    text-align:center;
`;


export {ContainerInactivate, Image,H3, H5}