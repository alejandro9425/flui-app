import styled from 'styled-components';


const BotonDangerState = styled.button `
    width: 110px;
    margin: auto;
    background: #dc3545;
    padding: 8%;
    color: #FFFFFF;
    font-size: 15px;
    border: none;
    border-radius: 5px;
    text-align: center;
    &:hover{
        background: #86232d;
    }
`;

const BotonSuccessState = styled.button `
    width: 110px;
    margin: auto;
    background: #28a745;
    padding: 8%;
    color: #FFFFFF;
    font-size: 15px;
    border: none;
    border-radius: 5px;
    text-align: center;
    &:hover{
        background: #19692b;
    }
`;

export {BotonDangerState, BotonSuccessState}