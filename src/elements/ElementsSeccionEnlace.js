import styled from 'styled-components';

const Container = styled.div`
    width: 60%;
    margin: auto;
`;

const Titulo = styled.h3`
    text-transform: uppercase;
    text-align:center;
`;

const Seccion = styled.div`
    display:flex;
    flex-direction:column;
`;

const EnlaceSeccion = styled.div`
    padding: 2%;
    text-align:center;
`;

const Enlace = styled.a`

`;
const ContainerTitle = styled.div`
    display:flex;
    justify-content:center;
`;
const InputTitle = styled.input`
    width: 70%;
    border:none;
    outline:none;
    border-bottom: 1px solid #D5D5D5;
    text-align:center;
    padding:5px;
    margin-bottom: 2%;
`;
const I = styled.i`

`;

const InputName = styled.input`
    width: 45%;
    border:none;
    outline:none;
    border-bottom: 1px solid #D5D5D5;
    text-align:center;
    margin-right: 2%;
    margin-bottom: 2%;
`;

const InputEnlace = styled.input`
    width: 30%;
    border:none;
    outline:none;
    border-bottom: 1px solid #D5D5D5;
    text-align:center;
    margin-bottom: 2%;
`;

export {
    Container,Titulo,Seccion,EnlaceSeccion, Enlace,
    InputTitle,ContainerTitle,I,InputName,InputEnlace
}