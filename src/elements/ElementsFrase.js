import styled from 'styled-components';

const ContainerFrase = styled.div`
    display: flex;
    flex-direction:column;
    width: 95%;
    margin: auto;
    margin-top: 10%;
    margin-bottom: 2%;
    background: #FFFFFF;
`;

const H2 = styled.h2`
    margin-top: 2%;
    width: 100%;
    text-align:center;
`;

const ContainerBody = styled.div`
    width: 95%;
    margin:auto;
    border-top: 1px solid #D3D3D3;

    margin-top: 2%;
    margin-bottom: 3%;
`;

const GroupContainer = styled.div`
    width:100%;
    display: flex;
    justify-content: space-around;
`;

const Article = styled.article`
    padding: 3%;
    text-align: center;
    word-wrap: break-word;
    width: 75%;
    border-bottom: 1px solid #D3D3D3;
`;

const I = styled.i`
    height: 1%;
    margin-top:3%;
`;

const Btn = styled.i`
    height: 1%;
    margin-top: 4%;
`;
const TextArea = styled.textarea`
    margin:3%;
    text-align: center;
    word-wrap: break-word;
    width: 75%;
    min-height: 70px;
    max-height: 70px;
    border: none;
    border-bottom: 1px solid #D3D3D3;
    outline:none;
`;



export {ContainerFrase, H2,ContainerBody,Article,I, GroupContainer,TextArea,Btn}