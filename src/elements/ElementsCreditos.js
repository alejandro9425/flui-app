import styled from 'styled-components';

const Container = styled.div`
    width: 90%;
    margin:auto;
    margin-top: 3%;
    margin-bottom: 5%;
`;

const Table = styled.table`
    width: 100%;
    
`;

const THead = styled.thead`
    border-bottom: 2px solid #0f1f39;
    text-align:center;
`;

const TBody = styled.tbody`

`;

const Th = styled.th`
    padding: 1%;
`;

const Tr = styled.tr`
   
`;
const Td = styled.td`
    padding: 1%;
    border-bottom: 1px solid #0f1f39;
    &.img {
        width: 25% !important;
    }
    &.des{
        width: 25%;
        text-align:center;
    }
    &.title{
        width: 25%;
        text-align:center;
    }
`;


const Img = styled.img`
    width: 55%;
    height: 150px;
    display:block;
    margin:auto;
`;

export {
    Container,Table,THead,TBody,Th,Tr,Td,Img
}