import styled from 'styled-components';

const Contenedor = styled.div`
    width: 80%;
    display:flex;
    justify-content:space-between;
    margin: auto;
`;

const Container = styled.div`
    width: 49%;
    display:flex;
`;
const ContainerImage = styled.div`
    width: 49%;
    display:flex;
    flex-direction:column;
`;
const Imagen = styled.img`margin:auto; width:70%; height:400px;`;
const SeccionItems = styled.div`
    width: 100%; 
    display:flex; 
    flex-direction:column; 
    justify-content:space-between;
`;

const Video = styled.iframe`
    height: 245px;
    margin-bottom: 1%;
`;

const EnlaceVideo = styled.a`

`;

const ImagenVideo = styled.img`
    height: 600px;
    margin-bottom: 1%;
`;
    

const Article = styled.article`
    text-align:justify;
    padding: 1%;
`;

const Enlace = styled.div`
    width: 80%;
    margin: auto;
    margin-top: 2%;
    margin-bottom: 2%;
    a{
        padding: 0.3%;
        text-align:center;
        text-decoration:none;
    }
    input{
        width: 37%;
        margin-left: 1%;
    }
`;
const Link = styled.a`
`;

const I = styled.i`
    margin-left: 1%;
`;

const ContEditVideo = styled.div`
    width: 100%;
    input{
        width: 90%;
    }
`;

const ContEditImagen = styled.div`
    width: 100%;
    display:flex;
    justify-content:end;
    input{
        width: 90%;
    }
`;
const ContentEditTitle = styled.div`
    width:80%;
    margin:auto;
    padding:2px;
    margin-bottom: 1%;
    display:flex;
    justify-content:center;
`;
const Title = styled.h3`
    text-transform: uppercase;
    text-align:center;
    margin-bottom: 2%;
`;

const InputTitle = styled.input`
    width: 95%;
    margin: auto;
    border:none;
    padding:5px;
    outline:none;
    border-bottom: 2px solid #D3D3D3;
    text-align:center;
`;

export {
    Contenedor,Container,Imagen,SeccionItems,Video, 
    Article, Enlace, Link,ImagenVideo, I, ContEditVideo,ContainerImage,ContEditImagen,
    Title,ContentEditTitle,InputTitle,EnlaceVideo
}