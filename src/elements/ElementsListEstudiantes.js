import styled from 'styled-components';

const Container = styled.div`
    width: 90%;
    margin:auto;
    margin-top: 3%;
    margin-bottom: 5%;
`;

const H2 = styled.h2`

`;

const Table = styled.table`
    width: 100%;
    margin:auto;
`;

const THead = styled.thead`

`;

const TBody = styled.tbody`

`;

const Tr = styled.tr`

`;
const Th = styled.th`
    text-align:center;
    border-bottom: 1px solid #000000;
`;
const Td = styled.td`
    padding: 1%;
    text-align:center;
    border-bottom: 2px solid #D3D3D3;
`;
const Img = styled.img`
    border-radius: 50%;
    width: 45%;
    height: auto;
`;

const ContainerHead = styled.div`
    display: flex;
    align-items:center;
    justify-content:space-around;
    margin-bottom:2%;
    a{
        text-decoration:none;
    }
`;

export {
    Container, H2, Table, THead, TBody, Tr, Th, Td, Img, ContainerHead
}