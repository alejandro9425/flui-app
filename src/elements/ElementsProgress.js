import styled from 'styled-components';

const Container = styled.div`
    width: 95%;
    margin:auto;
    margin-top: 3%;
    margin-bottom: 5%;
`;

const Table = styled.table`
    width: 100%;
`;

const TBody = styled.tbody``;
const THead = styled.thead`
    border-bottom: 1px solid #000000;
`;
const Tr = styled.tr``;
const Th = styled.th`
    text-align:center;
    &.td-progress{
        width:250px;
    }
`;
const Td = styled.td`
    padding: 1%;
    text-align:center;
`;

const Img = styled.img`
    border-radius: 50%;
    width: 45%;
    height: auto;
`;

const ContProgress = styled.div`
    width: 100%;
    height: 1.5em;
    position: relative;
    background-color: #D3D3D3;
    border-radius: 0 5px 5px 0;
`;//#0f1f39

const BarProgress = styled.div`
    background-color: #0f1f39;
    color: #FFFFFF;
    border-radius: 0 5px 5px 0;
`;

const ContReturn = styled.div`
    display:flex;
    padding:1%;
    justify-content:space-between;
    margin-top:1%;
    margin-bottom:1%;
    a{
        text-decoration:none;
    }
`;

const Curso = styled.h3`

`;

export {
    Container,Table,TBody,THead,Tr,Th,Td, ContProgress,BarProgress,
    ContReturn,Curso,Img
}