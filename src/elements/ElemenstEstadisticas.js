import styled from 'styled-components';

const Container = styled.div`
    width: 90%;
    margin:auto;
    margin-top: 3%;
    margin-bottom: 5%;
`;

const ContainerGrafica = styled.div`
    width: 75%;
    margin:auto;
    margin-bottom: 5%;
    padding-bottom: 3%;
    border-bottom: 2px solid #D3D3D3;
    canvas{
        width: 75% !important;
        margin:auto;
    }
    
`;

const Hr = styled.hr`
    
`;
const Title = styled.h3`
    text-align:center;
`;

export {
    Container,Hr, Title, ContainerGrafica
}