import styled from 'styled-components';

const TitleCenter = styled.h1`
    width: 75%;
    margin:auto;
    font-size: 28px;
    text-align:center;
    margin-top: 5%;
`;

const Ul = styled.ul`
    margin:auto;
    margin-top:5%;
    display:flex;
    width:75%;
    justify-content:space-around;
    border-bottom: 1px solid #09124f;
`;

const Li = styled.ul`
    > a{
        text-decoration:none;
    }
    i{
        padding:5px;
        :hover{
            cursor: pointer;
        }
    }
    >.active{
        padding:5px;
        color:#FFFFFF;
        background:#09124f;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -moz-border-radius-topleft: 5px;
        -moz-border-radius-topright: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;        
    }
`;

const Image = styled.img`
    width:55%;
    height:280px;
    display:block;
    margin:auto;
`;

const Article = styled.article`
    width: 80%;
    text-align:justify;
    margin:auto;
    margin-top: 5%;
    margin-bottom: 5%;
`;


const ConteinerGaleria = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    width: 80%;
    margin:auto;
    
    img{
      width: 40%;
      margin-bottom: 5%;
    }

    div{
        width: 40% !important;
        margin:auto;
        margin-bottom: 5% !important;
        display:flex !important;
        height:301px;
        justify-content:center !important;
        align-items: center !important;
    }
`;

const  ConteinerVacio = styled.div`
    
`;

const ConteinerVideos = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    width: 80%;
    margin:auto;
    div{
        width: 40%;
        margin-bottom: 5%;
        display:flex;
        justify-content:center;
        align-items: center;
    }
`;

const ComponentVideo = styled.iframe`
    width: 43%;
    height: 35vh;
    margin-bottom:5%;
`;

const Hr = styled.hr`
    width: 80%;
    margin:auto;
    margin-bottom:3%;
`;

const ContenerdorBtnEdit = styled.div`
    width: 80%;
    margin:auto;
    display:flex;
    justify-content:end;
    margin-top: 1%;
    margin-bottom: 2%;
    button{
        width: 8%;
        border:none;
        color: #FFFFFF;
    }
`;

const FormularioAdd = styled.form`
    width : 70%;
    margin:auto;
    margin-top: 1%;
    display:flex;
    flex-direction:column;
    label{
        margin-top:1%;
        text-align:center;
    }
    input{
        width: 30%;
        margin:auto;
        margin-top:2%;
        border:none;
        outline:none;
        border-bottom: 2px solid #D3D3D3;
        text-align:center;
        color:#000000;
    }
    button{
        width: 5%;
        margin:auto;
        margin-top:2%;
        border:none;
        background: #0e850e;
        color:#FFFFFF;
    }
`;

export {
    TitleCenter, Ul,Image,Article,ConteinerGaleria, ConteinerVacio,
    ConteinerVideos,ComponentVideo,Hr, ContenerdorBtnEdit,FormularioAdd,Li
}