import styled from 'styled-components';

const ContainerPerfil = styled.div`
    display: flex;
    width: 95%;
    margin: auto;
    margin-top: 2%;
    margin-bottom: 2%;
    background: #FFFFFF;

    @media (max-width: 414px){
        flex-direction: column;
    }
    @media (max-width: 375px){
            flex-direction: column;
    }
    @media (max-width: 360px){
            flex-direction: column;
    }

`;

const HeaderPerfil = styled.div`
    padding: 10px;
    width: 35%;
    border-right: 1px solid #d3d3d3;

    @media (max-width: 414px){
            width: 100%;
    }
    @media (max-width: 375px){
            width: 100%;
    }
    @media (max-width: 360px){
            width: 100%;
    }
`;

const Image = styled.img`
    width: 100%;
`;

const DescriptionPerfil = styled.div`
    margin-top: 3%;
    display: flex;
    flex-direction: column;
`;

const Article = styled.article`
    padding: 10px;
    text-align: justify;
    word-wrap: break-word;
`;

const BodyPerfil = styled.div`
    width: 65%;
    padding: 10px;
    
    @media (max-width: 414px){
        width: 100%;
    }
    @media (max-width: 375px){
        width: 100%;
    }
    @media (max-width: 360px){
        width: 100%;
    }
`;

const H2 = styled.h2`
    text-align: center;
`;

const GroupItem = styled.div`
    padding: 2%;
    display: flex;
    border-bottom: 1px solid #D3D3D3;
    vertical-align: middle;

    @media (max-width: 1156px){
        flex-direction: column;
    }

`;

const ItemP = styled.p`
    width: 15%;
    font-weight: bold;
    word-wrap: break-word;
    @media (max-width: 1156px){
        width: 100%;
        padding-left: 5%;
    }

    @media (max-width: 414px){
        width: 100%;
        padding-left: 0 ;
    }
    @media (max-width: 375px){
        width:100%;
        padding-left:0;
    }
    @media (max-width: 360px){
        padding-left: 0;
    }
`;

const GroupItemInput = styled.div`
    width: 85%;
    display: flex;
    justify-content: space-around;

    @media (max-width: 360px){
        flex-direction:column;
    }

`;

const ItemPerfil = styled.p`
    width: 70%;
    text-align: justify;
    word-wrap: break-word;
    @media (max-width: 414px){
        width: 100%;
    }
    @media (max-width: 375px){
        width: 100%;
    }
    @media (max-width: 360px){
        width: 100%;
    }
`;

const GroupItemInputPerfil = styled.div`
    padding: 3%;
    display: flex;
    border-bottom: 1px solid #D3D3D3;
    vertical-align: middle;
`;

const GroupInputItem = styled.div`
    width: 95%;
    display: flex;
    align-items: center;
    @media (max-width: 414px){
        display: flex;
        flex-direction: column;    
    }
    @media (max-width: 375px){
        display:flex;
        flex-direction:column;
    }

    @media (max-width: 300px){
        display: flex;
        flex-direction: column;
    }
`;

const Input = styled.input`
    text-align: center;
    font-size: 16px;
    font-weight: 200;
    padding: 10px 0;
    width: 100%;
    border: none #FFFFFF;
    background: transparent;
    border-bottom: 1px solid #D3D3D3;
    outline: none;
    &:focus{
        border: none !important;
        border-bottom: 1px solid #D3D3D3 !important;
    }
`;


const Select = styled.select`
    width: 100%;
    text-align:center;
`;

const Option = styled.option`
    
`;

const BtnControls = styled.div`
    display: flex;
    @media (max-width: 375px){
        margin-top: 2%;
    }

    @media (max-width: 300px){
        margin-top: 2%;
    }
`;

const DescArea = styled.textarea`
    border: none;
    outline: none;
    background: transparent;
    border-bottom: 1px solid #D3D3D3;
    min-width: 100%;
    max-height: 80px;
    min-height: 80px;
    &:focus{
        border: none;
        border-bottom: 1px solid #D3D3D3;
    }
`;

const contAsig = styled.div`
    display: flex;
    background:red;
`;

const SpanWarning = styled.span `
    width: 100%;
    margin:auto;
    margin-top: 2%;
    background: #fff3cd;
    text-align: center;
    color: #dc4e96;
`;

export { ContainerPerfil,HeaderPerfil,Image,DescriptionPerfil,Article,
         BodyPerfil,H2,GroupItem,ItemP,GroupItemInput,ItemPerfil,GroupItemInputPerfil,GroupInputItem,
         Input,BtnControls,DescArea,SpanWarning,contAsig, 
         Select, Option
        }