export const alertDeleteQuestion = {
    title: "Estas seguro de eliminar?",
    text: "Una vez eliminada la pregunta no podras recuperarla!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
}

export const deleteQuestion = "Poof! Tu Pregunta ha sido eliminada"

export const dontDeleteQuestion = "Tranquil@! la pregunta no se ha borrado"