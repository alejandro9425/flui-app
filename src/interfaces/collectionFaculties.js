export const faculdates = [
    {
        facultad: 'Facultad de Ingeniería',
    },
    {
        facultad: 'Facultad de Ciencias Económicas y Administrativas',
    },
    {
        facultad: 'Facultad de Ciencias Naturales y Matemáticas',
    },
    {
        facultad: 'Facultad de Derecho y Ciencias Políticas',
    },
    {
        facultad: 'Facultad de Humanidades, Artes y Ciencias Políticas',
    }
]