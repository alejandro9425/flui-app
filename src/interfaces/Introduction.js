export const Introduction = {
    picture: 'https://firebasestorage.googleapis.com/v0/b/trabajogrado-77f48.appspot.com/o/Image%20Default%2FIntroduccion.png?alt=media&token=f02be715-cb49-4466-8965-8a583b9f92b3',
    description: 'Es una breve descripción del tema o actividad que se va a realizar en la seccion. En otras palabras, lo que el estudiante debe tener encuenta para poder realizar la actividad.'
}