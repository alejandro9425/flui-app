import gus from '../assets/img/Gustavo.jpeg'
import dr from '../assets/img/Dr_Sara.png'
import videomovie from '../assets/img/5884-video-movie.gif'
import imgpicture from '../assets/img/5856-image-picture.gif'
import videocont from '../assets/img/69-video-content-ta-dah.gif'
import A from '../assets/img/alejandro.jpeg'
import J from '../assets/img/J.jpg'
export const collectionSeccion = {
    seccionUno: 'Seccion1',
    seccionDos: 'Seccion2',
    seccionTres: 'Seccion3',
    seccionCuatro: 'Seccion4'
}

export const urlImgAsig = {
    url: 'https://firebasestorage.googleapis.com/v0/b/trabajogrado-77f48.appspot.com/o/Image%20Default%2FdefaultAsignatura.png?alt=media&token=60614ef9-937a-466b-88f7-6dc518d73241'
}

export const collectionCreditos = [
    {
        id: 1,
        autor: 'Gustavo Martinez Villalobos',
        description: 'Profesor Asociado e investigador del grupo GESE - Unibagué.',
        link: 'gustavo.martinez@unibague.edu.co',
        img: gus
    },
    {
        id: 2,
        autor: 'Sara Torres Hernández',
        description: 'Jefa del departamento de investigación del Centro Chihuahuense de Estudios de Posgrado',
        link: 'sara.torres.h@cchep.edu.mx',
        img: dr
    }
    ,
    {
        id: 3,
        autor: 'Alejandro Luna Miranda',
        description: 'Asistente de investigación, Desarrollador Aplicación Web',
        link: '2220131011@estudiantesunibague.edu.co',
        img: A
    }
    ,
    {
        id: 4,
        autor: 'Jorge David Sigindioy Muchavisoy',
        description: 'Asistente de investigación, Desarrollador App Movil',
        link: '2220151021@estudiantesunibague.edu.co',
        img: J
    }
]

export const collectionContribuciones = [
    {
        id: 1,
        autor: 'LottieFiles',
        description: 'Image / Picture',
        link: 'https://lottiefiles.com/5856-image-picture',
        img: imgpicture
    },
    {
        id: 2,
        autor: 'LottieFiles',
        description: 'Video content ta-dah!',
        link: 'https://lottiefiles.com/69-video-content-ta-dah',
        img: videocont
    },
    {
        id: 3,
        autor: 'LottieFiles',
        description: 'Video / Movie',
        link: 'https://lottiefiles.com/5884-video-movie',
        img: videomovie
    }
]