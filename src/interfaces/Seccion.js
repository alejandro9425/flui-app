export const  Seccion = {
    content: false,
    title: 'Titulo Sección',
    picture: '',
    video: '',
    description: 'Es una breve descripción del tema o actividad que se va a realizar en la seccion. En otras palabras, lo que el estudiante debe tener encuenta para poder realizar la actividad.',
    link: ''
}