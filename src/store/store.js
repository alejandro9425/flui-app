import {createStore, combineReducers,applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

import {authReducer} from '../reducers/authReducer';
import {perfilReducer} from '../reducers/perfilReducer';
import {fraseDelDiaReducer} from '../reducers/fraseDelDiaReducer';
import {asignaturasReducer} from '../reducers/asignaturasReducer';
import {temasReducer} from '../reducers/temasReducer';
import {introduccionReducer,seccion1Reducer,seccion2Reducer,
        seccion3Reducer,seccion4Reducer,seccion5Reducer} from '../reducers/seccionesReducer';
import { estudiantesReducer } from '../reducers/estudiantesReducer';
import { docentesReducer } from '../reducers/docentesReducer';

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducer = combineReducers({
    auth: authReducer,
    perfil: perfilReducer,
    docentes: docentesReducer,
    frase: fraseDelDiaReducer,
    asignaturas: asignaturasReducer,
    temas: temasReducer,
    introduccion: introduccionReducer,
    seccion1: seccion1Reducer,
    seccion2: seccion2Reducer,
    seccion3: seccion3Reducer,
    seccion4: seccion4Reducer,
    seccion5: seccion5Reducer,
    estudiantes: estudiantesReducer
})

export const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(thunk))
)