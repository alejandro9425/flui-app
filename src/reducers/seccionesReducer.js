import {types} from '../types/types';
const initialState = {
    seccion: []
}

export const seccion1Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.loadSeccion1:
            return {
                id: action.payload.id,
                titulo: action.payload.titulo,
                descripcion: action.payload.descripcion,
                imagen: action.payload.imagen,
                video: action.payload.video,
                enlace: action.payload.enlace,
                contenido: action.payload.contenido
            }
        case types.uploadSeccion1Video:
            return {
                ...state,
                video: action.payload.videolink
            }
        case types.uploadSeccion1description:
            return {
                ...state,
                descripcion: action.payload.descripcion
            }
        case types.uploadSeccion1link:
            return {
                ...state,
                enlace: action.payload.enlace
            }
        case types.uploadSeccion1Picture:
            return {
                ...state,
                imagen: action.payload.imagen
            }
        case types.uploadSeccion1Titulo:
            return {
                ...state,
                titulo: action.payload.titulo
            }
        case types.logoutSeccion1:
            return {}
        default:
            return state;
    }
}


export const seccion2Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.loadSeccion2:
            return {
                id: action.payload.id,
                titulo: action.payload.titulo,
                descripcion: action.payload.descripcion,
                imagen: action.payload.imagen,
                video: action.payload.video,
                enlace: action.payload.enlace,
                contenido: action.payload.contenido
            }
        case types.uploadSeccion2Titulo:
            return {
                ...state,
                titulo: action.payload.titulo
            }
        case types.uploadSeccion2Video:
            return {
                ...state,
                video: action.payload.videolink
            }
        case types.uploadSeccion2description:
            return {
                ...state,
                descripcion: action.payload.descripcion
            }
        case types.uploadSeccion2link:
            return {
                ...state,
                enlace: action.payload.enlace
            }
        case types.uploadSeccion2Picture:
            return {
                ...state,
                imagen: action.payload.imagen
            }
        case types.logoutSeccion2:
            return {}
        default:
            return state;
    }
}

export const seccion3Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.loadSeccion3:
            return {
                id: action.payload.id,
                titulo: action.payload.titulo,
                descripcion: action.payload.descripcion,
                imagen: action.payload.imagen,
                video: action.payload.video,
                enlace: action.payload.enlace,
                contenido: action.payload.contenido
            }
        case types.uploadSeccion3Titulo:
            return {
                ...state,
                titulo: action.payload.titulo
            }
        case types.uploadSeccion3Video:
            return {
                ...state,
                video: action.payload.videolink
            }
        case types.uploadSeccion3description:
            return {
                ...state,
                descripcion: action.payload.descripcion
            }
        case types.uploadSeccion3link:
            return {
                ...state,
                enlace: action.payload.enlace
            }
        case types.uploadSeccion3Picture:
            return {
                ...state,
                imagen: action.payload.imagen
                }
        case types.logoutSeccion3:
            return {}
        default:
            return state;
    }
}

export const seccion4Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.loadSeccion4:
            return {
                id: action.payload.id,
                titulo: action.payload.titulo,
                nombrelink1: action.payload.nombrelink1,
                link1: action.payload.link1,
                nombrelink2: action.payload.nombrelink2,
                link2: action.payload.link2,
                contenido: action.payload.contenido
            }
        case types.refreshNameLink1:
            return {
                ...state,
                link1: action.payload.link,
                nombrelink1: action.payload.nombrelink
            }
        case types.refreshNameLink2:
            return {
                ...state,
                link2: action.payload.link,
                nombrelink2: action.payload.nombrelink
            }
        case types.refreshLinksTitle:
            return {
                ...state,
                titulo: action.payload.titulo
            }
        case types.logoutSeccion4:
            return {}
        default:
            return state;
    }
}

export const seccion5Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.loadSeccion5:
            return {
                ...state,
                seccion: [...action.payload]
            }
        case types.uploadQuestion:
            return {
                ...state,
                seccion: state.seccion.map(
                    e => (e.id === action.payload.id) ? action.payload : e
                )
            }
        case types.refreshQuestion:
            return {
                ...state,
                seccion: [...state.seccion, action.payload]
            }
        case types.delQuestion:
            return {
                ...state,
                seccion: state.seccion.filter(question => question.id !== action.payload)
            }
        case types.logoutSeccion5:
            return {}
        default:
            return state;
    }
}

export const introduccionReducer = (state = [], action) => {
    switch (action.type) {
        case types.loadIntroduccion:
            return {
               id: action.payload.id,
               imagen: action.payload.imagen,
               descripcion: action.payload.descripcion,
               contenido: action.payload.contenido
            }
        case types.introduccionSetDescripcion:
            return {
                ...state,
                descripcion: action.payload.description
            }
        case types.introduccionSetImagen:
            return {
                ...state,
                imagen: action.payload.imagen
            }
        case types.logoutIntroduccion:
            return {}
        default:
            return state;
    }
}