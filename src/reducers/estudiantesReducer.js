import {types} from '../types/types';
const initialState = {
    estudiantes: []
}
export const estudiantesReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.studentsLoad:
            return {
                ...state,
                estudiantes: [...action.payload]
            }
        case types.studentsLogoutClieaning:
            return {}
        default:
            return state;
    }
}