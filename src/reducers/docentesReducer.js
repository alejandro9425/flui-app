import {types} from '../types/types';
const initialState = {
    docentes: [],
    activate: null
}
export const docentesReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.docentesLoad:
            return {
                ...state,
                docentes: [...action.payload]
            }
        case types.docentesLogoutClieaning:
            return {
                ...state,
                activate: null,
                docentes: []
            }
        default:
            return state;
    }
}