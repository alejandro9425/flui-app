import {types} from '../types/types';
const initialState = {
    asignaturas: []
}
export const asignaturasReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.asignaturaLoad:
            return {
                ...state,
                asignaturas: [...action.payload],
            }
        case types.asignaturaAdd:
            return{
                ...state,
                asignaturas: [...state.asignaturas, action.payload]
            }
        case types.asignaturaUpdate:
            return {
                ...state,
                asignaturas: state.asignaturas.map(
                    e => (e.id === action.payload.id) ?
                        action.payload
                    :
                        e
                )
            }
        case types.refreshCantidadThemas:
            return {
                ...state,
                asignaturas: state.asignaturas.map(
                    e => (e.id === action.payload.id) ?
                        action.payload
                    :
                        e
                )
            }
        case types.refreshImagenAsig:
            return {
                ...state,
                asignaturas: state.asignaturas.map(
                    e => (e.id === action.payload.id) ?
                        action.payload
                    :
                        e
                )
            }

        case types.asignaturaLogoutClieaning:
            return {}
        default:
            return state;
    }
}