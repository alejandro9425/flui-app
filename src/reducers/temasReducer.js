import {types} from '../types/types';
const initialState = {
    temas: []
}

export const temasReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.asignaturaLoadTema:
            return {
                ...state,
                temas: [...action.payload],
            }
        case types.asignaturaAddTema:
            return{
                ...state,
                temas: [...state.temas, action.payload]
            }
        case types.temaIdLoading:
            return {
                tema: [action.payload]
            }
        case types.temaDel:
            return {
                ...state,
                temas: state.temas.filter(e => e.id !== action.payload)
            }
        case types.temaUpdate:
            return {
                ...state,
                temas: state.temas.map(
                    e => (e.id === action.payload.id) ?
                        action.payload
                    :
                        e
                )
            }
        case types.temasLogoutClieaning:
            return {}
        default:
            return state;
    }
}