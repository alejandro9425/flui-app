import {types} from '../types/types';

export const fraseDelDiaReducer = (state = {}, action) => {
    switch (action.type) {

        case types.fraseLoad:
            return {
                sentence: action.payload.sentence,
                uid: action.payload.uid
            }
        case types.fraseUpdate:
            return {
                ...state,
                sentence: action.payload.sentence,
                uid: action.payload.uid
            }
        case types.fraseLogoutClieaning:
            return {}
        default:
            return state;
    }
}