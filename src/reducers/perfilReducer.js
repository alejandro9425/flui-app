import {types} from '../types/types';

export const perfilReducer = (state = {}, action) => {
    switch (action.type) {
        case types.perfilLoad:
            return {
                uid: action.payload.uid,
                displayName: action.payload.displayName,
                state: action.payload.state,
                email: action.payload.email,
                photoURL: action.payload.photoURL,
                faculty: action.payload.faculty,
                program: action.payload.program,
                subjects: action.payload.subjects,
                rol: action.payload.rol,
                description: action.payload.description,
                gender: action.payload.gender
            }
        case types.perfildescription:
            return {
                ...state,
                description: action.payload.description
            }
        case types.perfilDisplayName:
            return{
                ...state,
                displayName: action.payload.displayName
            }
        case types.perfilAsignatura:
            return{
                ...state,
                subjects: action.payload.subjects
            }
        case types.perfilPhotoUpdate:
            return{
                ...state,
                photoURL: action.payload.url
            }
        case types.perfilFacultadUpdate:
            return {
                ...state,
                faculty: action.payload.faculty
            }
        case types.perfilProgramaUpdate:
            return {
                ...state,
                program: action.payload.program
            }
        case types.perfilGenderUpdate:
            return {
                ...state,
                gender: action.payload.gender
            }
        case types.perfilLogoutClieaning:
            return {}
        default:
            return state;
    }
}