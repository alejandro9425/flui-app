import React from 'react'
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { startRegisterWithEmailPasswordName } from '../../helpers/auth';
import { removeError, setError } from '../../actions/ui';
import {Contenedor, ContenedorLogin, ContenedorLoginHeader,
    ContenedorInputs,GroupInputs,Input, Boton, GroupLinks} from '../../elements/ElementsAuthentication';
import { useForm } from '../../hooks/useForm';
const RegisterScreen = () => {
    const dispatch = useDispatch()
    const [formValues, handleInputChange] = useForm({
        name: 'nombre test',
        codigo: 'test.test', 
        password: '123456',
        password2: '123456'
    });

    const {name, codigo, password, password2} = formValues;

    const handleRegister  = (e) => {
        e.preventDefault();
        if (isFormValid()) {
            let correo = codigo+'@unibague.edu.co';
            dispatch(startRegisterWithEmailPasswordName(correo, password, name))
        }
    }

    const isFormValid = ( ) =>{
        if (name.trim().length === 0) {
            dispatch(setError('Nombre requerido'))
            return false;
        }else if (password !== password2 || password.length < 5) {
            dispatch(setError('password No valido'))
            return false;
        }
        dispatch(removeError());
        return true;
    }


    return (
        <Contenedor>
            <ContenedorLogin>
                <ContenedorLoginHeader>
                    <h2>Registrarse</h2>
                </ContenedorLoginHeader>
                <ContenedorInputs>
                    <form onSubmit={handleRegister}>
                        <GroupInputs>
                            <Input
                                type="text"
                                name="codigo"
                                className="form-control mb-2"
                                placeholder="Usuario"
                                value={codigo}
                                onChange={handleInputChange}
                            />
                            <Input 
                                type="password"
                                name="password"
                                className="form-control mb-2"
                                placeholder="Contraseña"
                                value={password}
                                onChange={handleInputChange}
                            />
                            <Input 
                                type="password"
                                name="password2"
                                className="form-control mb-2"
                                placeholder="Ingrese Nuevamente la contraseña"
                                value={password2}
                                onChange={handleInputChange}
                            />
                        </GroupInputs>
                        <Boton type="submit">Registrarse</Boton>
                        <GroupLinks>
                            <Link to="/auth/login">Tengo cuenta</Link>
                        </GroupLinks>
                    </form>
                </ContenedorInputs>
            </ContenedorLogin>
        </Contenedor>
    )
}
export default RegisterScreen