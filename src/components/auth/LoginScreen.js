import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom';
import { startLoginEmailAndPassword, startLoginEmailPassword } from '../../helpers/auth';
import {Contenedor, ContenedorLogin, ContenedorLoginHeader,ContenedorInputs,
    GroupInputs,Input, Boton,GroupLinks, Elementlink, AlertAuth} from '../../elements/ElementsAuthentication'
import { useForm } from '../../hooks/useForm';
const LoginScreen = () => {
    const [errorLogin, setErrorLogin] = useState(false)
    const [messageError, setMessageError] = useState('')
    const dispatch = useDispatch();
    const [formValues, handleInputChange] = useForm({codigo: '', password: ''});
    const {codigo, password} = formValues;
    const loginWithEmailPassword = (e) => {
        e.preventDefault();
        let correo = codigo+'@unibague.edu.co';
        startLoginEmailAndPassword(correo,password)
        .then(user => {
            dispatch(startLoginEmailPassword(user))
        })
        .catch(e => {
            setErrorLogin(true);
            setMessageError(e.message)
        })
    }
    useEffect(() => {
        setTimeout(() => {
            setErrorLogin(false)
            setMessageError('')
        },10000)
        return () => {}
    }, [errorLogin])
    
    return (
        <Contenedor>
        <ContenedorLogin>
            <ContenedorLoginHeader>
                 <h2>Docentes</h2>
            </ContenedorLoginHeader>
            <ContenedorInputs>
                 <form onSubmit={loginWithEmailPassword}>
                     <GroupInputs>
                         <Input
                            placeholder="Usuario"
                            type="text"
                            name="codigo"
                            value={codigo}
                            onChange={handleInputChange}
                         />
                     </GroupInputs>
                     <GroupInputs>
                         <Input 
                            placeholder="Contraseña" 
                            type="password" 
                            name="password"
                            value={password}
                            onChange={handleInputChange}          
                         />
                     </GroupInputs>
                     {
                         errorLogin
                         ?
                            <AlertAuth>{messageError}</AlertAuth>
                         : null
                     }
                     <Boton
                        type="submit"
                     >Iniciar Sesión</Boton>
                     <GroupLinks>
                         <Link to="/auth/register">No tienes cuenta ?</Link>
                         <Elementlink href="/">Olvidaste la contraseña ?</Elementlink>
                     </GroupLinks>
                 </form>

             </ContenedorInputs>
        </ContenedorLogin>
    </Contenedor>
    )
}
export default LoginScreen