import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startLogout } from '../../../helpers/auth';
import { NavLink } from 'react-router-dom';
const Header = () => {

    const dispatch = useDispatch();
    const { displayName, rol, state } = useSelector(state => state.perfil)
    const { asignaturas } = useSelector(state => state.asignaturas)
    const handleLogout = () => {
        dispatch(startLogout());
    }
    return (//#fab400 amarillo
        <nav className="navbar navbar-expand-lg" 
            style={{background:'#0f1f39', color:'#FFFFFF'}}>
            <div className="container-fluid">
                <span className="navbar-brand">{displayName}</span>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className=" navbar-nav me-auto mb-2 mb-lg-0">
                            
                        {
                            rol === 'admin' &&
                            <li className="nav-item" >
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/docentes" style={{color:'#FFFFFF'}}>Docentes</NavLink>
                            </li>
                        }

                        {
                            rol === 'docente' &&
                            state &&
                            <>
                                <li className="nav-item" >
                                        <NavLink className="nav-link" 
                                        aria-current="page" to="/home" style={{color:'#FFFFFF'}}>Inicio</NavLink>
                                </li>
                                <li className="nav-item" >
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/Reflexiones" style={{color:'#FFFFFF'}}>Reflexiones</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/perfil" style={{color:'#FFFFFF'}}>Perfil</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/crear-asignatura" style={{color:'#FFFFFF'}}>Crear Asignatura</NavLink>
                                </li>
                                <li className="nav-item dropdown">
                                    <a
                                        className="nav-link dropdown-toggle"
                                        aria-current="page"
                                        id="navbarDropdown"
                                        data-bs-toggle="dropdown" aria-expanded="false"
                                        href="false"
                                        style={{color:'#FFFFFF'}}
                                    >Asignaturas
                                </a>
                                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        {
                                            asignaturas?.map((asignatura) => (
                                                <li key={asignatura.id}>
                                                    <NavLink
                                                        className="dropdown-item"
                                                        aria-current="page"
                                                        to={`/asignatura/${asignatura.id}`}
                                                        
                                                    >
                                                        {asignatura.nameSubject}
                                                    </NavLink>
                                                </li>
                                            ))
                                        }

                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/estadisticas" style={{color:'#FFFFFF'}}>Estadisticas</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/contribuciones" style={{color:'#FFFFFF'}}>Contribuciones</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/flui" style={{color:'#FFFFFF'}}>Flui</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link" 
                                    aria-current="page" to="/creditos" style={{color:'#FFFFFF'}}>Creditos</NavLink>
                                </li>
                            </>
                        }
                    </ul>
                    <div className="nav-iz">
                        <h3 className="btn btn-danger" onClick={handleLogout} >Salir</h3>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Header