import React from 'react'
import Header from './layouts/Header'
import {Container, Img} from '../../elements/ElementsHome'
import unibague from '../../assets/img/universidad.jpg'
const Home = () => {
    return (

        <>
            <Header />
           <Container>
                <Img src={unibague} />
           </Container>
        </>

    )
}
export default Home