import React, { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router'
import Header from '../layouts/Header'
import { TitleCenter, Ul } from '../../../elements/ElementsAulaAsignatura'
import { getAsignaturaId } from '../../../helpers/asignaturas'
import { startLoadTemas } from '../../../actions/temas';
import MenuTemas from './components/MenuTemas';
import TemaContenido from './TemaContenido';
const AsignaturaTema = () => {
    const dispatch = useDispatch();
    const { email } = useSelector(state => state.perfil);
    const { idAsignatura,idTema } = useParams();
    const [dataAsig, setDataAsig] = useState();
    const { asignaturas } = useSelector(state => state.asignaturas);
    const {temas} = useSelector(state => state.temas);
    const asignatura = useMemo(() => getAsignaturaId(idAsignatura,asignaturas), [idAsignatura, asignaturas]);
    
    useEffect(() => {
        asignatura.then(data =>{
            setDataAsig(data);
        })
        dispatch(startLoadTemas(email, idAsignatura))
    }, [asignatura, email, idAsignatura, dispatch])
    
    return (
        <>
            <Header />
            <TitleCenter>{dataAsig?.nombreAsignatura}</TitleCenter>
            <Ul>
                {
                    temas.length === 0 ? 
                    <h4>Cargando o No hay datos</h4>
                    :
                    <>
                        {
                            temas?.map((tema, index) => (
                                <MenuTemas
                                    key={index}
                                    asignaturaId={idAsignatura}
                                    temaId={tema.id}
                                    nombreTema={tema.name}
                                    active={true}
                                />
                            ))
                        }
                    </>
                }
            </Ul>
            <hr />
            <TemaContenido
                temaId={idTema}
            />
        </>
    )
}

export default AsignaturaTema
