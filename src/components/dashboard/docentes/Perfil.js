import React, { useState } from 'react'
import Header from '../layouts/Header'
import {
    ContainerPerfil, HeaderPerfil, Image, DescriptionPerfil, Article, BodyPerfil, H2,
    GroupItem, ItemP, GroupItemInput, ItemPerfil,// GroupItemInputPerfil, GroupInputItem,
    Input, BtnControls, DescArea, GroupItemInputPerfil, GroupInputItem, SpanWarning, Select, Option, 
} from '../../../elements/ElementsPerfil'
import { useDispatch, useSelector } from 'react-redux'
import { seleccionarPhoto, setDescription,setDisplayName, setAsignaturas, setFacultad, setPrograma, setGender } from '../../../helpers/perfil'
import {setPerfilDescription, setPerfilDisplayName, setPerfilAsignaturas, updatePhotoPerfil, setPerfilFacultad, setPerfilPrograma, setPerfilGender} from '../../../actions/perfil';
import { faculdates } from '../../../interfaces/collectionFaculties'
const Perfil = () => {
    const dispatch = useDispatch();
    const {photoURL, displayName,email, subjects, faculty,program, description, gender} = useSelector(state => state.perfil);
    const {sentence} = useSelector(state => state.frase);
    const [editPhoto, setEditPhoto] = useState(false);
    const [editDescription , setEditDescription ] = useState(false);
    const [newDescription, setNewDescription] = useState();
    const [newFacultad, setNewFacultad] = useState();
    const [newProgram, setNewProgram] = useState();
    const [editName, setEditName] = useState(false);
    const [newName, setNewName] = useState();
    const [editAsignatura, setEditAsignatura] = useState(false);
    const [editFacultad, setEditFacultad] = useState(false);
    const [editPrograma, setEditPrograma] = useState(false);
    const [newAsignatura, setNewAsignatura] = useState();
    const [editGenero, setEditGenero] = useState(false);
    const [newGenero, setNewGenero] = useState(gender);

    const handleSaveFacultad = () => {
        setFacultad(newFacultad,email)
        .then(() => {     
            dispatch(setPerfilFacultad(newFacultad));
            setNewFacultad(faculty);
            setEditFacultad(true);
        })
        .catch(e => {
            console.log(e);
        })
    }

    const handleSaveProgram = () => {
        setPrograma(newProgram,email)
        .then( async () => {     
            await dispatch(setPerfilPrograma(newProgram));
            setNewProgram();
            setEditPrograma(true);
        })
        .catch(e => {
            console.log(e);
        })  
    }

    const handleSaveDesc = (desc,pemail) => {
        setDescription(desc, pemail)
        .then(() => {
            dispatch(setPerfilDescription(desc));
        })
        .catch(e => {
            console.log(e);
        })
    }

    const handleSaveDisplayName = (name, pemail) => {
        setDisplayName(name, pemail)
        .then(() => {
            dispatch(setPerfilDisplayName(name))
        })
        .catch((e) => {
            console.log(e);
        })
    }

    const handleAsignaturas = (asig, pemail) => {
        setAsignaturas(asig, pemail)
        .then(() => {
            dispatch(setPerfilAsignaturas(asig))
        })
        .catch((e) => {
            console.log(e);
        })
    }

    const handlePhotoPerfil = (imagen, email) =>{
        seleccionarPhoto(imagen, email)
        .then(() =>{
            const url = URL.createObjectURL(imagen.target.files[0]);
            dispatch(updatePhotoPerfil(url))
        })
    }

    const handleSaveGenero = () => {
        setGender(parseInt(newGenero),email)
        .then(() => {
            dispatch(setPerfilGender(parseInt(newGenero)))
            setEditGenero(false)
        })
        .catch(e => console.log(e))
    }

    return (
        <>
            <Header />
            <ContainerPerfil>
                <HeaderPerfil>
                    <Image src={photoURL} />
                        {
                            editPhoto === false ?
                            <i className="btn far fa-edit" onClick={() => setEditPhoto(!editPhoto)}></i>
                            :
                            <>
                                <label htmlFor="exampleFormControlFile1"><i className="btn fas fa-upload" ></i></label>
                                <Input type="file"
                                    className="form-control-file"
                                    id="exampleFormControlFile1"
                                    name="image"
                                    style={{ display: 'none' }}
                                    onChange={e => handlePhotoPerfil(e, email)}
                                />
                                <i className="btn fas fa-window-close" onClick={() => setEditPhoto(!editPhoto)}></i>
                            </>
                        }
                    <DescriptionPerfil>
                        {
                            editDescription === false ?
                            <>
                                <Article>{description}</Article>
                                <i className="btn far fa-edit" 
                                    onClick={() => setEditDescription(!editDescription)}
                                ></i>
                            </>
                            :
                            <>
                                <DescArea
                                    defaultValue={description}
                                    onChange={(e) => setNewDescription(e.target.value)}
                                ></DescArea>

                                <BtnControls>
                                <i className="btn fas fa-save" 
                                    onClick={() => handleSaveDesc(newDescription,email)}
                                ></i>
                                <i className="btn fas fa-window-close"  
                                    onClick={() => setEditDescription(!editDescription)}
                                ></i>


                                </BtnControls>
                            </>
                        }
                        
                    </DescriptionPerfil>
                </HeaderPerfil>
                <BodyPerfil>
                    <H2>Acerca de mi</H2>
                    {
                        editName === false ?
                            <GroupItem>
                                <ItemP>Nombre: </ItemP>
                                <GroupItemInput>
                                    <ItemPerfil>{displayName}</ItemPerfil>
                                    <i className="btn far fa-edit" onClick={() => setEditName(!editName)}></i>
                                </GroupItemInput>
                            </GroupItem>
                        :
                        <GroupItemInputPerfil>
                            <GroupInputItem>
                                <Input type="text" defaultValue={newName || displayName} onChange={(e) => setNewName(e.target.value)} />
                                <BtnControls>
                                    <i className="btn fas fa-save" onClick={() => handleSaveDisplayName(newName, email)}></i>
                                    <i className="btn fas fa-window-close" onClick={() => setEditName(!editName)}></i>
                                </BtnControls>
                            </GroupInputItem>


                        </GroupItemInputPerfil>
                    }

                    
                    <GroupItem>
                        <ItemP>Correo: </ItemP>
                        <GroupItemInput>
                            <ItemPerfil className="email">{email}</ItemPerfil>
                            <i className="btn far fa-envelope" ></i>
                        </GroupItemInput>                       
                    </GroupItem>
                    {
                        editAsignatura === false ?
                            <GroupItem>
                                <ItemP>Asignaturas: </ItemP>
                                <GroupItemInput>
                                    <ItemPerfil>{subjects}</ItemPerfil>
                                    <i className="btn far fa-edit" onClick={() => setEditAsignatura(!editAsignatura)} ></i>
                                </GroupItemInput>
                            </GroupItem>
                        :
                            <GroupItem style={{ flexDirection: 'column' }}>
                                <GroupInputItem>
                                    <Input type="text" defaultValue={newAsignatura || subjects} onChange={(e) => setNewAsignatura(e.target.value)} />
                                        <BtnControls>
                                            <i className="btn fas fa-save" onClick={() => handleAsignaturas(newAsignatura,email)}></i>
                                            <i className="btn fas fa-window-close" onClick={() => setEditAsignatura(!editAsignatura)} ></i>
                                            
                                        </BtnControls>
                                </GroupInputItem>
                                    <SpanWarning>Separe cada asignatura con comas  “ , ” </SpanWarning>
                            </GroupItem>
                    }
                    

                    <GroupItem>
                        <ItemP>Frase del dia: </ItemP>
                        <GroupItemInput>
                            <ItemPerfil>{sentence}</ItemPerfil>
                            <i className="btn far fa-edit" style={{ color: '#FFFFFF' }}></i>
                        </GroupItemInput>
                    </GroupItem>
                    {
                        editFacultad === false
                        ?
                            <GroupItem>
                                <ItemP>Facultad: </ItemP>
                                <GroupItemInput>
                                    <ItemPerfil>{faculty}</ItemPerfil>
                                    <i className="btn far fa-edit" onClick={() => setEditFacultad(!editFacultad)}></i>
                                </GroupItemInput>
                            </GroupItem>
                        :
                            <GroupItem style={{ flexDirection: 'column' }}>
                                <GroupInputItem>
                                        <Select defaultValue={newFacultad || faculty} onChange={(e) => setNewFacultad(e.target.value)}>
                                            <Option value='nn'>Seleccione su Facultad</Option>
                                            {
                                                faculdates.map((facultad, index) => (
                                                    <Option value={facultad.facultad} key={index}>{facultad.facultad}</Option>
                                                ))
                                            }
                                        </Select>
                                        <BtnControls>
                                            <i className="btn fas fa-save" onClick={() => handleSaveFacultad()}></i>
                                            <i className="btn fas fa-window-close" onClick={() => setEditFacultad(!editFacultad)} ></i>
                                            
                                        </BtnControls>
                                </GroupInputItem>
                            </GroupItem>
                    }
                    {
                        editPrograma === false
                        ?
                            <GroupItem>
                                <ItemP>Programa: </ItemP>
                                <GroupItemInput>
                                    <ItemPerfil>{program}</ItemPerfil>
                                    <i className="btn far fa-edit" onClick={() => setEditPrograma (!editPrograma)}></i>
                                </GroupItemInput>
                            </GroupItem>
                        :
                            <GroupItem style={{ flexDirection: 'column' }}>
                                <GroupInputItem>
                                    <Input type="text" defaultValue={newProgram || program} onChange={(e) => setNewProgram(e.target.value)} />
                                        <BtnControls>
                                            <i className="btn fas fa-save" onClick={() => handleSaveProgram()}></i>
                                            <i className="btn fas fa-window-close" onClick={() => setEditPrograma (!editPrograma)} ></i>
                                            
                                        </BtnControls>
                                </GroupInputItem>
                            </GroupItem>
                    }
                                        {
                        editGenero === false
                        ?
                            <GroupItem>
                                <ItemP>Genero: </ItemP>
                                <GroupItemInput>
                                    <ItemPerfil>{gender === '' ? 'Seleccione su genero': gender === 1 ? 'Masculino' : 'Femenina'}</ItemPerfil>
                                    <i className="btn far fa-edit" onClick={() => setEditGenero(!editGenero)}></i>
                                </GroupItemInput>
                            </GroupItem>
                        :
                            <GroupItem style={{ flexDirection: 'column' }}>
                                <GroupInputItem>
                                        <Select value={newGenero} onChange={(e) => setNewGenero(e.target.value)}>
                                            <Option value='nn'>Seleccione su Genero</Option>
                                            <Option value='0'>Femenino</Option>
                                            <Option value='1'>Masculino</Option>

                                        </Select>
                                        <BtnControls>
                                            <i className="btn fas fa-save" onClick={() => handleSaveGenero()}></i>
                                            <i className="btn fas fa-window-close" onClick={() => setEditGenero(!editGenero)} ></i>
                                            
                                        </BtnControls>
                                </GroupInputItem>
                            </GroupItem>
                    }

                </BodyPerfil>
            </ContainerPerfil>
        </>
    )
}

export default Perfil