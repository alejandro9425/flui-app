import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { startUpdateFrase } from '../../../actions/frase';
import {
    ContainerFrase, H2, ContainerBody, Article,
    I, GroupContainer, TextArea, Btn
} from '../../../elements/ElementsFrase'
import Header from '../layouts/Header'
const FraseDelDia = () => {
    const dispatch = useDispatch();
    const {email} = useSelector(state => state.perfil)
    const {sentence, uid} = useSelector(state => state.frase)
    const [newfrase, setFrase] = useState();
    const [editFrase, setEditFrase] = useState(false);
    const handleUpdateFrase = () => {
        try {
            dispatch(startUpdateFrase(email,uid, newfrase))
            setEditFrase(false);
        } catch (error) {
            console.log(error)
        }
    }
    return (
        <>
            <Header />
            <ContainerFrase>
                <H2>Reflexión</H2>
                <ContainerBody>
                    {
                        editFrase === false ?
                            (
                                <GroupContainer>
                                    <Article>{sentence}</Article>
                                    <I className="btn far fa-edit" onClick={() => setEditFrase(!editFrase)}></I>
                                </GroupContainer>
                            )
                            :
                            (
                                <GroupContainer>
                                    <TextArea name="frase" defaultValue={newfrase || sentence} onChange={(e)=> setFrase(e.target.value) } />
                                    <Btn className="btn fas fa-save" onClick={handleUpdateFrase}></Btn>
                                    <Btn className="btn fas fa-window-close" onClick={() => setEditFrase(!editFrase)}></Btn>
                                </GroupContainer>
                            )
                    }
                </ContainerBody>
            </ContainerFrase>     
        </>
    )
}

export default FraseDelDia