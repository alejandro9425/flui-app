import React from 'react'
import { ContainerGrafica, Title } from '../../../../elements/ElemenstEstadisticas'
import {Bar,Pie} from 'react-chartjs-2'
import {backgroundColor,borderColor} from '../../../../interfaces/CollectionGraficas'
const Grafica = ({title,labels,data,label,type}) => {
    const dataBar = {
        labels: labels,
        datasets: [
          {
            label: label,
            data: data,
            backgroundColor,
            borderColor,
            borderWidth: 1,
          },
        ],
      };
    const options = {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
            },
          ],
        },
      };
    
    const dataPie = {
        labels: labels,
        datasets: [
          {
            label: label,
            data: data,
            backgroundColor,
            borderColor,
            borderWidth: 1,
          },
        ],
      };

    return (
        <ContainerGrafica>
            <Title>{title}</Title>
            {
                type === 'Bar'
                ?
                <Bar data={dataBar} options={options} />
                :
                type === 'Pie'
                ?
                <Pie className='bough' data={dataPie} />
                :
                <></>
            }
            
        </ContainerGrafica>
    )
}

export default Grafica
