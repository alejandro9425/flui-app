import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import { comments } from '../../../../helpers/consultas';
import Header from '../../layouts/Header'
import { Link } from 'react-router-dom'
import { Contenedor, Ul, Li, Table, Th,Tr, Thead,Tbody} from '../../../../elements/ElementsRespuestas';
import DataComentario from './DataComentario';
const Comentarios = () => {
    const {email} = useSelector(state => state.perfil)
    const [preguntas, setPreguntas] = useState()
    const {idAsignatura,idTema, idpregunta} =  useParams();

    useEffect(() => {
      comments(email,idAsignatura,idpregunta+'?').then(data => {
        setPreguntas(data)
      })
    },[email,idAsignatura,idpregunta])
    return (
        <>
          <Header />
          <Contenedor>
            <Ul>
              <Li>Pregunta: {idpregunta+'?'}</Li>
              <Li>
                <Link 
                  to={`/asignatura/${idAsignatura}/${idTema}`}
                  className='fas fa-arrow-circle-left'
                > Regresar</Link>
              </Li>
            </Ul>
            <Table>
              <Thead>
                <Tr>
                  <Th>Nombre</Th>
                  <Th>Correo</Th>
                  <Th>Respuesta</Th>
                  <Th>Fecha</Th>
                  <Th className='calificacion'>Calificación</Th>
                </Tr>
              </Thead>
              <Tbody>
                {
                  preguntas?.map((pregunta, index) => (
                    <DataComentario
                      key={index}
                      pregunta={pregunta}
                    />
                  ))
                }

              </Tbody>
            </Table>
          </Contenedor>

          
        </>
    )
}

export default Comentarios
