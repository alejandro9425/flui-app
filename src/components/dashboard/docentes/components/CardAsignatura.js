import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {Card, CardSeccion, CardTitle,Input,
        SeccionCard, Separador, TextSeccion, 
        I, CardFooter, Imagen, CardSeccionD, Inputedit, CardView} from '../../../../elements/ElementsAsignatura'
import { seleccionarPhotoAsignatura } from '../../../../helpers/asignaturas'
import { refreshImagenAsignatura, startUpdateNameAsignatura } from '../../../../actions/asignaturas'
import { Link } from 'react-router-dom'
const CardAsignatura = ({data}) => {
    const dispatch = useDispatch();
    const {email} = useSelector(state => state.perfil);
    const [loadingImagen, setLoadingImagen] = useState(false);
    const [loadingNameA, setLoadingNameA] = useState(false);
    const [asignaturaNew, setAsignaturaNew] = useState(data.nameSubject);
    const [editName, setEditName] = useState(false);
    
    const handleUpdatePhoto = (e) => {
        setLoadingImagen(true);
        seleccionarPhotoAsignatura(e,data.id,email, data.nameSubject)
        .then(() => {
            const url = URL.createObjectURL(e.target.files[0]);
            setLoadingImagen(false)
            dispatch(
                refreshImagenAsignatura(
                    data.id, data.nameSubject, data.comments, data.students
                    , url, data.likes, data.themes
                )
            );
        })
    }

    const handleUpdateAsignatura = () => {
        setLoadingNameA(true);
        try {
            dispatch(
                startUpdateNameAsignatura(
                    email,data.id,asignaturaNew,data.comments,data.students,data.photoURL, data.likes,data.themes)
                );
            setEditName(false);
            setLoadingNameA(false);
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Card>
            <CardSeccion>
                <CardTitle>
                    {
                        editName 
                        ?
                        <>
                            <Inputedit 
                                value={asignaturaNew || data.nameSubject} 
                                onChange={(e) => setAsignaturaNew(e.target.value)}
                            />
                            <I className="btn fas fa-save" onClick={handleUpdateAsignatura}></I>
                            <I className="btn fas fa-window-close" onClick={() => setEditName(!editName)}></I>
                        </>
                        :
                        <>
                            {data.nameSubject}
                            {
                                loadingNameA ?
                                <I className='spinner-border spinner-border-sm mt-2' />
                                :
                                <I className="btn far fa-edit" onClick={() => setEditName(!editName)}></I>
                            }
                            
                        </>
                        
                    }
                </CardTitle>
                <SeccionCard>
                    <TextSeccion>
                        <b>{data.themes}</b>
                        <i>Temas</i>
                    </TextSeccion>
                    <Separador><b>/</b></Separador>
                    <TextSeccion>
                        <b>{data.students}</b>
                        <i>Estudiantes</i>
                    </TextSeccion>
                </SeccionCard>
                <CardFooter>
                    <I className="far fa-thumbs-up"> | {data.likes}</I>
                    <I className="far fa-comment"> | {data.comments}</I>
                </CardFooter>
                <CardView>
                    <Link to={`/asignatura/${data.id}`}><i className="btn far fa-eye"></i></Link>
                    <Link to={`/estudiantes/asignatura/${data.id}/${asignaturaNew || data.nameSubject}`}><i className="btn fas fa-users"></i></Link>
                </CardView>
            </CardSeccion>
            <CardSeccionD>
                <Imagen
                    src={data.photoURL}
                />
                <div>

                    {
                        loadingImagen 
                        ?
                            <I className='spinner-border spinner-border-sm mt-2' />
                        :
                        <>
                            <label htmlFor={data.nameSubject}><i className="btn fas fa-upload" ></i></label>
                            <Input type="file"
                                className="form-control-file"
                                id={data.nameSubject}
                                name="image"
                                style={{ display: 'none' }}
                                onChange={e => handleUpdatePhoto(e)}
                            />
                        </>
                    }
                </div>
            </CardSeccionD>
            
        </Card>
    )
}

export default CardAsignatura
