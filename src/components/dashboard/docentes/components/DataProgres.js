import React from 'react'
import { Tr, Td, ContProgress, BarProgress, Img } from '../../../../elements/ElementsProgress';
import moment from 'moment'
import 'moment/locale/es'
moment.locale('es')
const DataProgres = ({data, cantTemas}) => {
    
    return (
        <Tr>
            <Td style={{width: '20%'}}><Img src={data.photoUrl}/></Td>
            <Td>{data.id}</Td>
            <Td>{data.nameUser}</Td>
            <Td>{moment(data.date).fromNow()}</Td>
            <Td>
                <ContProgress>
                   <BarProgress style={{width:`${(data.progress/cantTemas)*100}%`}}>
                       {((data.progress/cantTemas)*100).toFixed(0)+'%'}
                   </BarProgress>
                </ContProgress>
            </Td>
        </Tr>
    )
}

export default DataProgres
