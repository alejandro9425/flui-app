import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { refreshNameLink1, refreshNameLink2, refreshTitulo } from '../../../../actions/contenido'
import {Container, Seccion, Titulo,EnlaceSeccion, Enlace, InputTitle, I, ContainerTitle, InputName, InputEnlace} from '../../../../elements/ElementsSeccionEnlace'
import { setEnlace1, setEnlace2, setTitleEnlace } from '../../../../helpers/contenido'
const SeccionEnlace = ({seccion,seccionColeccion,idAsignatura,idTema,edit}) => {
    const {email} = useSelector(state => state.perfil)
    const dispatch = useDispatch();
    const [loadingTitle, setloadingTitle] = useState(false)
    const [loadingnamelink1, setloadingnamelink1] = useState(false)
    const [loadingnamelink2, setloadingnamelink2] = useState(false)
    const [editTitle, setEditTitle] = useState()
    const [editlink1, setEditlink1] = useState()
    const [editnameLink1, setEditnameLink1] = useState()
    const [editlink2, setEditlink2] = useState()
    const [editnameLink2, setEditnameLink2] = useState()

    const handleTitle = () => {
        setloadingTitle(true)
        setTitleEnlace(email,idAsignatura,idTema,seccionColeccion,seccion.id,editTitle)
        .then(() => {
            dispatch(refreshTitulo(editTitle))
            setloadingTitle(false)
        })
    }

    const handleNameLink1 = () => {
        setloadingnamelink1(true)
        setEnlace1(email,idAsignatura,idTema,seccionColeccion,seccion.id,editnameLink1,editlink1)
        .then(() => {
            dispatch(refreshNameLink1(editnameLink1,editlink1))
            setloadingnamelink1(false)
        })
    }

    const handleNameLink2 = () => {
        setloadingnamelink2(true)
        setEnlace2(email,idAsignatura,idTema,seccionColeccion,seccion.id,editnameLink2,editlink2)
        .then(() => {
            dispatch(refreshNameLink2(editnameLink2,editlink2))
            setloadingnamelink2(false)
        })
    }

    return (
        <Container>
            {
                edit
                ?
                <ContainerTitle>
                    <InputTitle  
                        placeholder='Escriba el titulo de esta sección'
                        defaultValue={editTitle || seccion?.titulo}
                        onChange={(e) => setEditTitle(e.target.value)}
                    />
                    {
                        loadingTitle
                        ?
                            <I className='spinner-border spinner-border-sm mt-2' />
                        :
                            <I className="btn fas fa-save" onClick={() => handleTitle()} />
                    }
                </ContainerTitle>
                :
                <Titulo>{editTitle || seccion?.titulo}</Titulo>
            }
            <Seccion>
                {
                    edit 
                    ?
                    <>
                        <EnlaceSeccion>
                            <InputName 
                                placeholder='Nombre del enlace'
                                defaultValue={editnameLink1 || seccion?.nombrelink1}
                                onChange={(e) => setEditnameLink1(e.target.value)}
                            />
                            <InputEnlace 
                                placeholder='www.enlace.com' 
                                defaultValue={editlink1 || seccion?.link1}
                                onChange={(e) => setEditlink1(e.target.value)}
                            />
                            
                            {
                                loadingnamelink1
                                ?
                                    <I className='spinner-border spinner-border-sm mt-2' />
                                :
                                    <I className="btn fas fa-save" onClick={() => handleNameLink1()} />
                            }
                        </EnlaceSeccion>
                        <EnlaceSeccion>
                        <InputName 
                                placeholder='Nombre del enlace'
                                defaultValue={editnameLink2 || seccion?.nombrelink2}
                                onChange={(e) => setEditnameLink2(e.target.value)}
                            />
                            <InputEnlace 
                                placeholder='www.enlace.com' 
                                defaultValue={editlink2 || seccion?.link2}
                                onChange={(e) => setEditlink2(e.target.value)}
                            />
                            
                            {
                                loadingnamelink2
                                ?
                                    <I className='spinner-border spinner-border-sm mt-2' />
                                :
                                    <I className="btn fas fa-save" onClick={() => handleNameLink2()} />
                            }
                        </EnlaceSeccion>
                    </>
                    :
                    <>
                        <EnlaceSeccion>
                            {editnameLink1 || seccion?.nombrelink1}: 
                            <Enlace href={editlink1 || seccion?.link1}> Click aqui</Enlace>
                        </EnlaceSeccion>
                        <EnlaceSeccion>
                            {editnameLink2 || seccion?.nombrelink2}: 
                            <Enlace href={editlink2 || seccion?.link2}> Click aqui</Enlace> 
                        </EnlaceSeccion>
                    </>
                }
            </Seccion>
        </Container>
    )
}

export default SeccionEnlace
