import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { deleteQuestionAction, setseccionQuestion } from '../../../../actions/contenido'
import swal from 'sweetalert'
import { I, InputPreguntaEdit, PreguntaBody, PreguntaContainer, PreguntaHeader, Text, TextPregunta } from '../../../../elements/ElementsPreguntas'
import { updateQuestion } from '../../../../helpers/temas'
import { alertDeleteQuestion, deleteQuestion, dontDeleteQuestion } from '../../../../interfaces/Alerts'
import { CantComments, delQuestion } from '../../../../helpers/consultas'
import { Link } from 'react-router-dom'
const PreguntaComponent = ({question,idQuestion,idAsignatura, idTema}) => {
    const [edit, setedit] = useState(false);
    const [questionEdit, setQuestionEdit] = useState(question);
    const [loadingQuestion, setloadingQuestion] = useState(false);
    const {email} = useSelector(state => state.perfil);
    const dispatch = useDispatch();
    const [cantComentarios, setCantComentarios] = useState(0)
    
    const handleUpdateQuestion = () => {
        setloadingQuestion(true);
        updateQuestion(email, idAsignatura, idTema,idQuestion,questionEdit)
        .then(() => {
            dispatch(setseccionQuestion(idQuestion, questionEdit));
            setloadingQuestion(false);
            setedit(false);
        })
    }

    const handleDeleteQuestion = (id) => {
        swal(alertDeleteQuestion)
          .then((willDelete) => {
            if (willDelete) {
              swal(deleteQuestion, {
                icon: "success",
              });
              delQuestion(email,idAsignatura,idTema,id)
              .then(() => dispatch(deleteQuestionAction(id)))
            } else {
              swal(dontDeleteQuestion);
            }
          });
    }

    useEffect(() => {
        CantComments(email,idAsignatura,questionEdit || question)
        .then(cantidad => {
            setCantComentarios(cantidad)
        })
        return () => {setCantComentarios()}
    }, [email, idAsignatura,question,questionEdit])

    return (
            <PreguntaContainer>
                <PreguntaHeader>
                    

                    {
                        edit
                        ?
                            <>
                                <InputPreguntaEdit 
                                    value={questionEdit || question}
                                    onChange={(e) => setQuestionEdit(e.target.value)}
                                />
                                {
                                    loadingQuestion 
                                    ?
                                        <I className='spinner-border spinner-border-sm mt-2' />
                                    :
                                        <>
                                            <I className="btn fas fa-save" onClick={()=>handleUpdateQuestion()} />
                                            <I className="btn fas fa-window-close" onClick={() => setedit(!edit)} />
                                        </>
                                }

                            </>
                        :
                            <>
                                <TextPregunta>{questionEdit}</TextPregunta>
                                <I className="btn far fa-edit" onClick={() => setedit(!edit)} />
                            </>
                    }
                    
                    <I 
                        className='fas fa-trash' 
                        onClick={() => handleDeleteQuestion(idQuestion)}
                    />
                </PreguntaHeader>
                <PreguntaBody>
                    <Text>Respuestas: {cantComentarios}</Text>
                    <Text>ver Respuestas: 
                    
                    <Link to={`/asignatura/respuestas/${idAsignatura}/${idTema}/${idQuestion}/${question}`}><I className="btn far fa-eye" /></Link>
                    </Text>
                </PreguntaBody>
            </PreguntaContainer>
    )
}

export default PreguntaComponent
