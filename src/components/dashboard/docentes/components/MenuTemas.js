import React from 'react'
import { NavLink } from 'react-router-dom'
import { Li } from '../../../../elements/ElementsMenuTopics'
const MenuTemas = ({asignaturaId,temaId,nombreTema,active}) => {
     return (
        <Li>
                <NavLink 
                    to={`/asignatura/${asignaturaId}/${temaId}`}>
                    {nombreTema}
                </NavLink>
        </Li>
    )
}

export default MenuTemas