import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { deleteThemes, refreshTemas } from '../../../../actions/temas';
import { Tr,Td, I, Input } from '../../../../elements/ElementsMenuTopics'
import { delTopics } from '../../../../helpers/consultas';
import { updateTopicName } from '../../../../helpers/temas';
import { NavLink } from 'react-router-dom';
const DataThemes = ({number,asignaturaId,temaId,nombreTema,date}) => {
    const dispatch = useDispatch();
    const [edit, setEdit] = useState(false)
    const [loading, setLoading] = useState(false)
    const [newNameTheme, setNewNameTheme] = useState(nombreTema)
    const {email} = useSelector(state => state.perfil);

    const handleDelete = (asignaturaId,temaId) => {
        delTopics(email,asignaturaId,temaId).then(()=> {
            dispatch(deleteThemes(temaId));
        })  
    }

    const handleSave = () => {
        setLoading(true);
        updateTopicName(email,asignaturaId,temaId,newNameTheme)
        .then(() => {
            dispatch(refreshTemas(temaId,date,newNameTheme));
            setLoading(false);
            setEdit(false);
        })
    }

    return (
        <Tr>
            <Td style={{width:'5%'}}>{number}</Td>
            <Td style={{width:'60%'}}>
                {
                    edit
                    ?
                    <Input value={newNameTheme} onChange={(e) => setNewNameTheme(e.target.value)}/>
                    :
                    <NavLink 
                        to={`/asignatura/${asignaturaId}/${temaId}`}>
                        {nombreTema}
                    </NavLink>
                    
                }
            </Td>
            <Td style={{width:'17.5%'}}>
                {
                    edit
                    ?
                        loading ?
                            <I className='spinner-border spinner-border-sm mt-2' />
                        :
                        <>
                            <I className="btn fas fa-save" onClick={() => handleSave()}></I>
                            <I className="btn fas fa-window-close" onClick={() => setEdit(!edit)}></I>
                        </>
                    :
                        <I className="btn far fa-edit" onClick={() => setEdit(!edit)}></I>
                }
                
                
                
                
            </Td>
            <Td style={{width:'17.5%'}}>
                <I className='btn fas fa-trash' onClick={() => handleDelete(asignaturaId,temaId)}></I>
            </Td>
        </Tr>
    )
}

export default DataThemes
