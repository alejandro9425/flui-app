import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { setIntroDescription, setIntroImagen } from '../../../../actions/contenido';
import {Image,Article} from '../../../../elements/ElementsAulaAsignatura'
import { Contenedor, ControlsButtons, TextArea, I } from '../../../../elements/ElementsEditContenido'
import { Input } from '../../../../elements/ElementsPerfil';
import { seleccionarPhoto, updateIntroduccion } from '../../../../helpers/contenido';
const Introduccion = ({introduccion, idAsignatura, idTema, edit}) => {
    const dispatch = useDispatch();
    const {email} = useSelector(state => state.perfil)
    const [valorintroduccion, setIntroduccion] = useState();
    const [loadingimagen, setLoadingImagen] = useState(false)
    const [loadingDescripcion, setLoadingDescripcion] = useState(false)
    
    const handlesetIntroduccion = () => {
        setLoadingDescripcion(true)
        updateIntroduccion(email,idAsignatura,idTema,introduccion.id,valorintroduccion)
        .then(() => {
            dispatch(setIntroDescription(valorintroduccion))
            setLoadingDescripcion(false)
        })
    }

    const handlesetImage = (imagen, email, idAsignatura, idTema, idSeccion) => {
        setLoadingImagen(true)
        seleccionarPhoto(imagen, email, idAsignatura, idTema, idSeccion)
        .then(() => {
            const url = URL.createObjectURL(imagen.target.files[0]);
            dispatch(setIntroImagen(url))
            setLoadingImagen(false)
        })
    }
    return (
        <>
            <Image src={introduccion.imagen} />
            {
                edit &&
                <ControlsButtons>
                    {
                        loadingimagen ?
                        <>
                            <i className='spinner-border spinner-border-sm mt-2'></i>
                        </>
                        :
                        <>
                            <label htmlFor="exampleFormControlFile1">
                                <i className="btn fas fa-upload" ></i>
                            </label>
                            <Input type="file"
                                className="form-control-file"
                                id="exampleFormControlFile1"
                                name="image"
                                style={{ display: 'none' }}
                                onChange={e => handlesetImage(e, email,idAsignatura, idTema,introduccion.id)}
                            />
                        </>
                    }
                </ControlsButtons>
            }
            {
                edit ?
                <Contenedor>
                    <TextArea onChange={(e) => setIntroduccion(e.target.value)} defaultValue={valorintroduccion || introduccion.descripcion}/>
                    {
                        loadingDescripcion ?
                        <div>
                            <i className='spinner-border spinner-border-sm mt-2 mb-2'></i>
                        </div>
                        :
                        <div>
                            <I className="btn fas fa-save" onClick={() => handlesetIntroduccion()} />
                        </div>
                    }
                </Contenedor>
                :
                <Article>
                    {introduccion.descripcion}
                </Article>
            }
        </>
    )
}

export default Introduccion
