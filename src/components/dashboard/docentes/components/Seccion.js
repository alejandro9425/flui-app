import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { setseccion1descripcion, setseccion1imagen, setseccion1link, setseccion1linkVideo, setseccion1Titulo, setseccion2descripcion, setseccion2imagen, setseccion2link, setseccion2linkVideo, setseccion2Titulo, setseccion3descripcion, setseccion3imagen, setseccion3link, setseccion3linkVideo, setseccion3Titulo } from '../../../../actions/contenido'
import { Div, TextArea } from '../../../../elements/ElementsEditContenido'
import { Input } from '../../../../elements/ElementsPerfil'
import { Article, Container, Contenedor, 
    Enlace, Imagen, SeccionItems, Link, ImagenVideo, I, ContEditVideo, ContainerImage, ContEditImagen, Title, ContentEditTitle, InputTitle, EnlaceVideo} from '../../../../elements/ElementsSeccion'
import { setSeccion1Description, setSeccion1Video, setSeccionDescripcion, setSeccionLink, setSeccionPicture } from '../../../../helpers/contenido'
import { collectionSeccion } from '../../../../interfaces/Collections';
import imagenVideo from '../../../../assets/img/69-video-content-ta-dah.gif'
import imgVideo from '../../../../assets/img/5884-video-movie.gif'
import imagenDefault from '../../../../assets/img/5856-image-picture.gif'
const Seccion = ({seccion, seccionColeccion, edit}) => {
    const dispatch = useDispatch();
    const [videolink, setVideoLink] = useState()
    const [descripcion, setDescripcion] = useState()
    const [enlace, setEnlace] = useState()
    const [titulo, setTitulo] = useState()
    const {email} = useSelector(state => state.perfil)
    const { idAsignatura,idTema } = useParams()

    const [loadingVideo, setloadingVideo] = useState(false)
    const [loadingDescripcion, setloadingDescripcion] = useState(false)
    const [loadingEnlace, setloadingEnlace] = useState(false)
    const [loadingImagen, setloadingImagen] = useState(false)
    const [loadingTitulo, setloadingTitulo] = useState(false)

    const handleVideo = () => {
        setloadingVideo(true)
        setSeccion1Video(email,idAsignatura,idTema,seccionColeccion, seccion.id,videolink)
        .then(() => {
            
            if (seccionColeccion === collectionSeccion.seccionUno) {
                dispatch(setseccion1linkVideo(videolink))
            }else if (seccionColeccion === collectionSeccion.seccionDos) {
                dispatch(setseccion2linkVideo(videolink))
            }else if (seccionColeccion === collectionSeccion.seccionTres) {
                dispatch(setseccion3linkVideo(videolink))
            }
            setloadingVideo(false)
        })
    }

    const handleDescription = () => {
        setloadingDescripcion(true);
        setSeccion1Description(email,idAsignatura,idTema,seccionColeccion, seccion.id,descripcion)
        .then(() => {
            
            if (seccionColeccion === collectionSeccion.seccionUno) {
                dispatch(setseccion1descripcion(descripcion))
            }else if (seccionColeccion === collectionSeccion.seccionDos) {
                dispatch(setseccion2descripcion(descripcion))
            }else if (seccionColeccion === collectionSeccion.seccionTres) {
                dispatch(setseccion3descripcion(descripcion))
            }
            setloadingDescripcion(false);
        })
    }

    const handleEnlace = () => {
        setloadingEnlace(true);
        setSeccionLink(email, idAsignatura, idTema,seccionColeccion, seccion.id, enlace)
        .then(() => {
            
            if (seccionColeccion === collectionSeccion.seccionUno) {
                dispatch(setseccion1link(enlace))
            }else if (seccionColeccion === collectionSeccion.seccionDos) {
                dispatch(setseccion2link(enlace))
            }else if (seccionColeccion === collectionSeccion.seccionTres) {
                dispatch(setseccion3link(enlace))
            }
            setloadingEnlace(false);
        })
    }

    const handleImagen = (pimagen) => {
        setloadingImagen(true);
        setSeccionPicture(email, idAsignatura, idTema,seccionColeccion, seccion.id, pimagen)
        .then(() => {
            const url = URL.createObjectURL(pimagen.target.files[0]);
            if (seccionColeccion === collectionSeccion.seccionUno) {
                dispatch(setseccion1imagen(url))
            }else if (seccionColeccion === collectionSeccion.seccionDos) {
                dispatch(setseccion2imagen(url))
            }else if (seccionColeccion === collectionSeccion.seccionTres) {
                dispatch(setseccion3imagen(url))
            }
            setloadingImagen(false)
        })
    }

    const handleTitulo = () => {
        setloadingTitulo(true)
        setSeccionDescripcion(email,idAsignatura,idTema,seccionColeccion,seccion.id,titulo)
        .then(() => {
            if (seccionColeccion === collectionSeccion.seccionUno) {
                dispatch(setseccion1Titulo(titulo))
            }else if (seccionColeccion === collectionSeccion.seccionDos) {
                dispatch(setseccion2Titulo(titulo))
            }else if (seccionColeccion === collectionSeccion.seccionTres) {
                dispatch(setseccion3Titulo(titulo))
            }
            setloadingTitulo(false)
        })
    }

    return (
        <>
        <ContentEditTitle>
        {
            edit ? 
            <>
                <InputTitle  
                    placeholder='Titulo de la sección'
                    defaultValue={titulo || seccion?.titulo}
                    onChange={(e) => setTitulo(e.target.value)}
                />
                {
                    loadingTitulo ? 
                    <I className='spinner-border spinner-border-sm mt-2' />
                    :
                    <I className="btn fas fa-save" onClick={() => handleTitulo()}/>
                }
            </>
            :
            <Title>{seccion.titulo}</Title>
        }
        </ContentEditTitle>
        
        <Contenedor>
                
            <Container>
                <SeccionItems>
                    {
                        seccion?.video === '' ?
                        <>
                            <ImagenVideo src={imagenVideo}/>
                            {
                                edit && 
                                (
                                    loadingVideo ? 
                                        <I className='spinner-border spinner-border-sm mt-2' />
                                    :
                                        <ContEditVideo>
                                            <input 
                                                type='text' 
                                                placeholder='Enlace del video'
                                                defaultValue={videolink || seccion?.video}
                                                onChange={(e) => setVideoLink(e.target.value)}
                                            />
                                            <I className="btn fas fa-save" onClick={() => handleVideo()} />
                                        </ContEditVideo>
                                )
                            }
                        </>
                        :
                        <>
                             <EnlaceVideo href={seccion?.video} target='_blank'>
                                <ImagenVideo src={imgVideo}/>
                            </EnlaceVideo>
                            {
                                edit &&
                                <ContEditVideo>
                                    <Input 
                                        type='text' 
                                        placeholder='Enlace del video'
                                        defaultValue={videolink || seccion?.video}
                                        onChange={(e) => setVideoLink(e.target.value)}
                                    />
                                    {
                                        loadingVideo ?
                                        <I className='spinner-border spinner-border-sm mt-2' />
                                        :
                                        <I className="btn fas fa-save" onClick={() => handleVideo()} />
                                    }
                                </ContEditVideo>                                
                            }
                        </>
                    }
                    {
                        edit ?
                        <>
                            <TextArea defaultValue={descripcion || seccion?.descripcion} onChange={(e) => setDescripcion(e.target.value)} />
                                {
                                    loadingDescripcion ?
                                    <Div>
                                        <I className='spinner-border spinner-border-sm mt-2 mb-2' />
                                    </Div>
                                    :
                                    <Div>
                                        <I className="btn fas fa-save" onClick={() => handleDescription()} />
                                    </Div>
                                }
                        </>
                        :
                        <Article>{seccion?.descripcion}</Article>
                    }
                </SeccionItems>
            </Container>
            <ContainerImage>
                    {
                        edit ?
                        <>
                            <Imagen src={seccion?.imagen} />
                            <ContEditImagen>
                               {
                                   loadingImagen ?
                                    <I className='spinner-border spinner-border-sm mt-2' />
                                   :
                                   <>
                                        <label htmlFor={seccionColeccion}>
                                            <i className="btn fas fa-upload" ></i>
                                        </label>
                                        <Input type="file"
                                            className="form-control-file"
                                            id={seccionColeccion}
                                            name="image"
                                            style={{ display: 'none' }}
                                            onChange={e => handleImagen(e)}
                                        />
                                   </>
                               }
                            </ContEditImagen>
                        </>
                        :
                        seccion?.imagen === ''
                        ?
                            <Imagen src={imagenDefault} />
                        :
                        <Imagen src={seccion?.imagen} />
                    }
            </ContainerImage>
        </Contenedor>
        <Enlace>
            {
                edit ?
                <>
                    <Input 
                        type='text' 
                        placeholder='Escriba aqui el enlace'
                        defaultValue={enlace || seccion?.enlace}
                        onChange={(e) => setEnlace(e.target.value)}
                    />
                    {
                        loadingEnlace ?
                            <I className='spinner-border spinner-border-sm mt-2' />
                        :
                        <>
                            <I className="btn fas fa-save" onClick={() => handleEnlace()} />
                        </>
                    }
                </>
                :
                <Link href={seccion?.enlace} target='_blank'>Enlace</Link>
            }
        </Enlace>
        </>
    )
}

export default Seccion
