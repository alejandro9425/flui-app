import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AddPregunta,I, Conteiner, Preguntas, Titulo, InputPregunta, ContPreguntas } from '../../../../elements/ElementsPreguntas'
import { addQuestion } from '../../../../helpers/temas'
import PreguntaComponent from './PreguntaComponent'
import { refreshNewPregunta } from '../../../../actions/contenido'
const SeccionPreguntas = ({edit,idAsignatura, idTema}) => {
    const dispatch = useDispatch()
    const [newQuestion, setNewQuestion] = useState('')
    const [loadNewQuestion, setLoadNewQuestion] = useState(false)
    const {seccion} = useSelector(state => state.seccion5)
    const {email} = useSelector(state => state.perfil)

    const handleAddPregunta = () => {
        setLoadNewQuestion(true)
        addQuestion(email,idAsignatura,idTema, '¿'+newQuestion+'?')
        .then(response => {
            dispatch(refreshNewPregunta(response.id, '¿'+newQuestion+'?'))
            setLoadNewQuestion(false)
            setNewQuestion('')
        })
    }

    return (
        <Conteiner>
            <Titulo>Preguntas</Titulo>
            <Preguntas>
                <AddPregunta>
                    <InputPregunta
                        placeholder='Pregunta'
                        value={newQuestion}
                        onChange={(e) => setNewQuestion(e.target.value)}
                    />
                    {
                        loadNewQuestion 
                        ?
                            <I className='spinner-border spinner-border-sm mt-2' />
                        :
                            <I className="btn fas fa-plus" onClick={() => handleAddPregunta()} />
                    }
                    
                </AddPregunta>
                <ContPreguntas>
                    {
                        seccion.map((data) => (                            
                            <PreguntaComponent
                                key={data.id}
                                idQuestion={data.id}
                                question={data.question}
                                idAsignatura={idAsignatura}
                                idTema={idTema}
                                edit={edit}
                            />
                        ))
                    }
                </ContPreguntas>
            </Preguntas>
        </Conteiner>
    )
}

export default SeccionPreguntas
