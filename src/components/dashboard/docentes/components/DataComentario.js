import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import {Input, Td, Tr, I} from '../../../../elements/ElementsRespuestas';
import { updateNotaQuestion } from '../../../../helpers/contenido';
import moment from 'moment'
import 'moment/locale/es'
moment.locale('es')
const DataComentario = ({pregunta}) => {
    const [editQ, setEditQ] = useState(false)
    const [loadingNota, setLoadingNota] = useState(false)
    const {email} = useSelector(state => state.perfil)
    const {idAsignatura} = useParams()
    const [nota, setnota] = useState(pregunta.qualification)
    const handleUpdateNota = (idComentario) => {
        setLoadingNota(true)
        updateNotaQuestion(email,idAsignatura,idComentario,nota)
        .then(() => {
            setLoadingNota(false)
            setEditQ(false)
        })   
    }
    
    return (
        <Tr>
        <Td>{pregunta.name}</Td>
        <Td>{pregunta.email}</Td>
        <Td>{pregunta.solveQuestion}</Td>
        <Td>{moment(pregunta.date).fromNow()}</Td>
        <Td className='calificacion'>
            {
              editQ 
              ?
                <>
                  <Input 
                    type='number'  
                    placeholder='0.0'
                    value={nota || pregunta.qualification}
                    onChange={(e) => setnota(e.target.value)}
                    min='0.0'
                    max='5.0'
                  />
                  {
                      loadingNota 
                      ?
                        <I className='spinner-border spinner-border-sm mt-2' />
                      :
                        <>
                        <I className="btn fas fa-save" onClick={() => handleUpdateNota(pregunta.id)}/>
                        <I className="btn fas fa-window-close" onClick={() => setEditQ(!editQ)} />
                        </>
                  }
                  
                </>
              :
               <>
                    { nota }
                    <I className="btn far fa-edit" onClick={() => setEditQ(!editQ)} />
               </>
            }
        </Td>
      </Tr>
    )
}

export default DataComentario
