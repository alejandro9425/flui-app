import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import { Table,TBody,THead,Tr,Th,Td, Container, ContReturn, Curso } from '../../../elements/ElementsProgress';
import { getCurso, getProgressStudent } from '../../../helpers/consultas';
import Header from '../layouts/Header';
import DataProgres from './components/DataProgres';
import { Link } from 'react-router-dom'
const Progress = () => {
    const {email} = useSelector(state => state.perfil)
    const {idAsignatura} = useParams();
    const [progreso, setProgreso] = useState();
    const [cantTemas, setCantTemas] = useState();
    const [nameCurso, setNameCurso] = useState();
    useEffect(() => {
        getProgressStudent(email,idAsignatura)
        .then(data => {
            setProgreso(data)
        })
        getCurso(email,idAsignatura)
        .then(data => {
            setCantTemas(data[0].themes)
            setNameCurso(data[0].nameSubject)
        })

    }, [email, idAsignatura])

    return (
        <>
            <Header />
            <Container>
                <ContReturn>
                    <Curso>Curso: {nameCurso}</Curso>
                    <Link 
                        className='fas fa-arrow-circle-left'
                        to={`/asignatura/${idAsignatura}`}
                    >Regresar</Link>
                </ContReturn>
                <Table>
                    <THead>
                        <Tr>
                            <Th>Foto</Th>
                            <Th>Correo</Th>
                            <Th>Nombre Estudiante</Th>
                            <Th>Ultimo progreso</Th>
                            <Th className='td-progress'>Progreso</Th>
                        </Tr>
                    </THead>
                    <TBody>
                        {
                            progreso === undefined 
                            ?
                               <Tr>
                                    <Td colSpan='5'>
                                        Cargando...
                                    </Td>
                               </Tr>
                            :
                            progreso.length === 0 ?
                                <Tr>
                                    <Td colSpan='5'>
                                        No hay progresos :(
                                    </Td>
                                </Tr>
                            :
                            
                            progreso.map((data, index) => (
                                <DataProgres
                                    key={index}
                                    data={data}
                                    cantTemas={cantTemas}
                                />
                            ))
                        }
                    </TBody>
                </Table>
            </Container>
        </>
    )
}
export default Progress
