import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { estudiantesLoad } from '../../../actions/asignaturas'
import { Container, H2, Table, THead,TBody,Tr,Th,Td, Img, ContainerHead } from '../../../elements/ElementsListEstudiantes'
import { getStudents } from '../../../helpers/consultas'
import Header from '../layouts/Header'
import { Link } from 'react-router-dom'
import moment from 'moment'
import 'moment/locale/es'
moment.locale('es')
const ListEstudiantes = () => {
    const dispatch = useDispatch();
    const {asig} = useParams();
    const {email} = useSelector(state => state.perfil)
    const {idAsignatura} = useParams();
    const {estudiantes} = useSelector(state => state.estudiantes)
    useEffect(() => {
        getStudents(email,idAsignatura)
        .then((data) => {
            dispatch(estudiantesLoad(data))
        })
    }, [email,idAsignatura,dispatch])
    
    return (
        <>
            <Header />
            <Container>
                <ContainerHead>
                    <H2>Lista de estudiantes del curso: {asig}</H2>
                    <Link 
                    to={`/crear-asignatura`}
                    className='fas fa-arrow-circle-left'
                    > Regresar</Link>
                </ContainerHead>
                <Table>
                    <THead>
                        <Tr>
                            <Th>Correo</Th>
                            <Th>Nombre</Th>
                            <Th>Fecha Matricula</Th>
                            <Th>Foto Perfil</Th>
                        </Tr>
                    </THead>
                    <TBody>
                        {
                            estudiantes?.map(estudiante => (
                                <Tr key={estudiante.id}>
                                    <Td>{estudiante.id}</Td>
                                    <Td>{estudiante.name}</Td>
                                    <Td>{moment(estudiante.date).fromNow()}</Td>
                                    <Td style={{width: '20%'}}>
                                        <Img src={estudiante.photoUrl} />
                                    </Td>
                                </Tr>
                            ))
                        }
                    </TBody>
                </Table>
            </Container>
        </>
    )
}

export default ListEstudiantes
