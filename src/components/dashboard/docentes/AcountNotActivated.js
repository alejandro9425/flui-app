import React from 'react'
import { ContainerInactivate, H3, H5, Image } from '../../../elements/ElementsAcountNotActivate'
import logounibague from '../../../assets/img/unibague.jpg'
import Header from '../layouts/Header'
const AcountNotActivated = () => {
    return (
        <>
        <Header />
        <ContainerInactivate>
            <Image src={logounibague}/>
            <H3>Cuenta no Activada</H3>
            <H5>Por favor intentelo en unos 5 minutos de lo contrario comuniquese con el administrador</H5>
        </ContainerInactivate>
        </>
    )
}

export default AcountNotActivated
