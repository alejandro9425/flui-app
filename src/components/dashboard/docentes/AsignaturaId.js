import React, { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { refreshthemes, startLoadTemas, startNewTema } from '../../../actions/temas';
import { TitleCenter,FormularioAdd } from '../../../elements/ElementsAulaAsignatura';
import { getAsignaturaId, updateNumberTemas } from '../../../helpers/asignaturas';
import { createTema } from '../../../helpers/temas';
import { useForm } from '../../../hooks/useForm';
import Header from '../layouts/Header';
import { Table, TBody, THead,Tr, Th, Td, ContProgress } from '../../../elements/ElementsMenuTopics'
import DataThemes from './components/DataThemes';
import { Link } from 'react-router-dom'
const AsignaturaId = () => {
    const dispatch = useDispatch();
    const { email } = useSelector(state => state.perfil)
    const [formValues, handleInputChange, reset] = useForm({ newTema: '' });
    const {temas} = useSelector(state => state.temas);
    const { idAsignatura } = useParams();
    const { newTema } = formValues;
    const { asignaturas } = useSelector(state => state.asignaturas);
    const [dataAsig, setDataAsig] = useState();
    const asignatura = useMemo(() => getAsignaturaId(idAsignatura,asignaturas), [idAsignatura, asignaturas]);
    useEffect(() => {
        asignatura.then(data =>{
            setDataAsig(data);
        })
        dispatch(startLoadTemas(email, idAsignatura))
    }, [asignatura, email, idAsignatura, dispatch])

    const handleSubmit = (e) => {
        e.preventDefault();
        var fecha = new Date().getTime();
        createTema(email, idAsignatura, newTema,fecha)
        .then(e => {
            dispatch(startNewTema(idAsignatura, newTema, fecha));
            dispatch(refreshthemes(idAsignatura,dataAsig?.nameSubject,
                dataAsig?.themes+1,dataAsig?.photoURL,dataAsig?.students,
                dataAsig?.comments,dataAsig?.likes));
            reset({ newTema: '' });
            updateNumberTemas(email,idAsignatura,dataAsig?.themes+1);
        })
        .catch(e => console.log('error'))
    }


    return (
        <>
            <Header />
            <TitleCenter>{dataAsig?.nameSubject}</TitleCenter>
            <FormularioAdd onSubmit={handleSubmit}>
                <input type='text' name='newTema' 
                    value={newTema} 
                    onChange={handleInputChange} 
                    placeholder='Agregar Nuevo Tema'
                />
                <button type='submit'> + </button>
            </FormularioAdd>
            <ContProgress>
            <Link 
                to={`/progress/asignatura/${idAsignatura}`}
                className="btn far fa-eye"
            > progreso
            </Link>
            </ContProgress>
            <Table>
                <THead>
                    <Tr>
                        <Th>#</Th>
                        <Th>Tema</Th>
                        <Th colSpan='2'>Accion</Th>
                    </Tr>
                </THead>
                <TBody>
                    {
                        temas?.length === 0 ?
                        <Tr>
                            <Td colSpan='3'>
                                Cargando o no hay temas
                            </Td>
                        </Tr>
                        :
                         temas?.map((tema, index) => (
                            <DataThemes
                                key={index}
                                number={index+1}
                                asignaturaId={idAsignatura}
                                temaId={tema.id}
                                nombreTema={tema.name}
                                date={tema.date}
                            />
                         ))
                    }
                </TBody>
            </Table>
        </>
    )
}

export default AsignaturaId
