import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Container } from '../../../elements/ElemenstEstadisticas'
import { Subjects } from '../../../helpers/consultas'
import Header from '../layouts/Header'
import Grafica from './components/Grafica'

const Estadisticas = () => {
    const {email} = useSelector(state => state.perfil)
    const [namesSubjects, setNamesSubjects] = useState()
    const [students, setStudents] = useState()
    const [likes, setLikes] = useState()
    useEffect(() => {
        Subjects(email)
        .then(data => {
            setNamesSubjects(data.nameAsig)
            setStudents(data.students)
            setLikes(data.likes)
        })
    }, [email])

    return (
        <>
            <Header />
            <Container>
                {
                    namesSubjects !== undefined &&
                    students !== undefined &&
                    <Grafica 
                        labels={namesSubjects}
                        label='Numero de Estudiantes'
                        title='Cantidad de estudiantes por Asignatura'
                        data={students}
                        type='Bar'
                    />
                }
                {
                    namesSubjects !== undefined &&
                    likes !== undefined &&
                    <Grafica 
                        labels={namesSubjects}
                        label='Likes por Asignatura'
                        title='Likes por Asignatura'
                        data={likes}
                        type='Pie'
                    />
                }            
            </Container>
        </>
    )
}

export default Estadisticas
