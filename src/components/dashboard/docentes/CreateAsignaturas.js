import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createAsignatura } from '../../../helpers/asignaturas';
import { useForm } from '../../../hooks/useForm';
import {startNewAsignatura} from '../../../actions/asignaturas';
import Header from '../layouts/Header';
import {    Button,  ContainerAddAsignatura, 
            ContainerCard, Form, Input, 
            Title } 
        from '../../../elements/ElementsAsignatura';
import CardAsignatura from './components/CardAsignatura';
import { urlImgAsig } from '../../../interfaces/Collections';
const CreateAsignaturas = () => {
    const dispatch = useDispatch();
    const {asignaturas} = useSelector(state => state.asignaturas)
    const {email} = useSelector(state => state.perfil)
    const [formValues, handleInputChange,reset] =  useForm({nameAsig: ''});
    const {nameAsig} = formValues;

    const handleSubmit = (e) => {
        e.preventDefault();
        createAsignatura(email,nameAsig)
        .then(response =>{
            dispatch(startNewAsignatura(response.id, nameAsig,urlImgAsig.url));
            reset({nameAsig: ''})
        })
        .catch(e => {
            console.log(e);
        })
    }

    return (
        <>
            <Header />
            <ContainerAddAsignatura>
                <Title>Asignaturas</Title>
                <Form onSubmit={handleSubmit}>
                    <Input 
                        type='text' 
                        name="nameAsig" 
                        value={nameAsig} 
                        onChange={handleInputChange}
                        placeholder='Nueva Asignatura'
                    />
                    <Button type='submit'>+ agregar</Button>
                </Form>
                <ContainerCard>
                    {
                        asignaturas?.map(asignatura => (
                            <CardAsignatura
                                key={asignatura.id}
                                data={asignatura}
                            />
                        ))
                    }
                </ContainerCard>
            </ContainerAddAsignatura>
        </>
    )
}

export default CreateAsignaturas