import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux'
import { setIntroduccion, setSeccion1, setSeccion2, setSeccion3, setSeccion4, setSeccion5 } from '../../../actions/contenido';
import Introduccion from './components/Introduccion';
import { Hr } from '../../../elements/ElementsAulaAsignatura'
import Seccion from './components/Seccion';
import SeccionEnlace from './components/SeccionEnlace';
import SeccionPreguntas from './components/SeccionPreguntas';
import { ControlsButtons } from '../../../elements/ElementsEditContenido';
import { collectionSeccion } from '../../../interfaces/Collections';
const TemaContenido = () => {
    const [stateEdit, setStateEdit] = useState(false);

    const {email} = useSelector(state => state.perfil);
    const dispatch = useDispatch();
    const { idAsignatura, idTema } = useParams();
    const introduccion = useSelector(state => state.introduccion)
    const seccionUno = useSelector(state => state.seccion1)
    const seccionDos = useSelector(state => state.seccion2)
    const seccionTres = useSelector(state => state.seccion3)
    const seccionCuatro = useSelector(state => state.seccion4)

    useEffect(() => {
        dispatch(setIntroduccion(email,idAsignatura,idTema))
        dispatch(setSeccion1(email,idAsignatura,idTema))
        dispatch(setSeccion2(email,idAsignatura,idTema))
        dispatch(setSeccion3(email,idAsignatura,idTema))
        dispatch(setSeccion4(email,idAsignatura,idTema))
        dispatch(setSeccion5(email,idAsignatura,idTema))
        return {}
    }, [email,idAsignatura, idTema,dispatch])



    return (
        <>
            <ControlsButtons>
                <button onClick={() => setStateEdit(!stateEdit)}>Editar</button>
            </ControlsButtons>
            
            {/* introduccion */}
                <Introduccion 
                    idAsignatura= {idAsignatura} 
                    idTema = {idTema}
                    introduccion={introduccion} 
                    edit={stateEdit}
                />
            {/* seccion 1 */}
                <Hr />
                <Seccion 
                    seccion={seccionUno} 
                    seccionColeccion={collectionSeccion.seccionUno} 
                    edit={stateEdit} 
                />

            {/* seccion 2 */}
                <Hr />
                <Seccion 
                    seccion={seccionDos} 
                    seccionColeccion={collectionSeccion.seccionDos} 
                    edit={stateEdit} 
                />
            {/* seccion 3 */}
                <Hr />
                <Seccion 
                    seccion={seccionTres} 
                    seccionColeccion={collectionSeccion.seccionTres} 
                    edit={stateEdit} 
                />
            {/* seccion 4 */}
                <Hr />
                <SeccionEnlace 
                    idAsignatura={idAsignatura}
                    idTema={idTema}
                    seccion={seccionCuatro}
                    seccionColeccion={collectionSeccion.seccionCuatro}
                    edit={stateEdit}
                />

            {/* seccion 5 */}
                <Hr />
                <SeccionPreguntas 
                    idAsignatura={idAsignatura} 
                    idTema={idTema}
                    edit={stateEdit}/>
        </>
    )
}

export default TemaContenido
