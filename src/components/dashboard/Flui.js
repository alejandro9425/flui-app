import React from 'react'
import Header from './layouts/Header'
import {Container, Article, Hr, Img, Img2} from '../../elements/ElementsFlui'
import flip from '../../assets/img/Flip.png'
import flipLearning from '../../assets/img/flheader.png'
const Flui = () => {
    return (
        <>
            <Header />
            <Container>
                
                <Img src={flipLearning} />
                <Hr />
                <Article>
                Una de las tendencias en innovación educativa, 
                que se está implementando en muchas instituciones, 
                es el denominado Flipped Learning (Aprendizaje inverso), 
                un modelo que también incentiva el interés por investigar, 
                la capacidad de hacer las cosas de otra manera, 
                la ambición de motivar a los estudiantes por un aprendizaje social y la necesidad 
                de mejorar la consecución de resultados de aprendizaje diversos, 
                que implica adquirir conocimientos disciplinares, su profunda comprensión, 
                la aptitud para saber cómo aplicarlos y transferirlos a nuevos entornos y situaciones. 
                Esta WebApp es uno de los productos de un proyecto de investigación que busca evaluar el 
                impacto del modelo FL (considerado evolutivo del Flipped Classroom), 
                y la metodología de enseñanza justo a tiempo (Just-in-Time Teaching), 
                que fomenta el estudio previo. 
                Éxitos!.
                </Article>
                <Hr />
                <Img2 src={flip} />
            </Container>
        </>
    )
}

export default Flui
