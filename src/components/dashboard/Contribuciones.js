import React from 'react'
import { Container, Table, TBody, Th, THead, Tr,Td, Img } from '../../elements/ElementsContribuciones'
import { collectionContribuciones } from '../../interfaces/Collections'
import Header from './layouts/Header'

const Contribuciones = () => {
    return (
        <>
            <Header/>
            <Container>
                <Table>
                    <THead>
                        <Tr>
                            <Th>#</Th>
                            <Th>Autor</Th>
                            <Th>Imagen</Th>
                            <Th>Descripción</Th>
                            <Th>Link</Th>
                        </Tr>
                    </THead>
                    <TBody>
                        {
                            collectionContribuciones.map((data,index) => (
                                <Tr key={index}>
                                    <Td>{data.id}</Td>
                                    <Td className='title'>{data.autor}</Td>
                                    <Td className='img'><Img src={data.img} /> </Td>
                                    <Td className='des'>{data.description}</Td>
                                    <Td>{data.link}</Td>
                                </Tr>
                            ))
                        }
                    </TBody>
                </Table>
            </Container>
        </>
    )
}

export default Contribuciones
