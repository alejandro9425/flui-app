import React from 'react'
import { Container, Table, TBody, Th, THead, Tr,Td, Img } from '../../elements/ElementsCreditos'
import { collectionCreditos } from '../../interfaces/Collections'
import Header from './layouts/Header'
const Creditos = () => {
    return (
        <>
            <Header />
            <Container>
                <Table>
                    <THead>
                        <Tr>
                            <Th>#</Th>
                            <Th>Autor</Th>
                            <Th>Imagen</Th>
                            <Th>Descripción</Th>
                            <Th>Correo</Th>
                        </Tr>
                    </THead>
                    <TBody>
                        {
                            collectionCreditos.map((data,index) => (
                                <Tr key={index}>
                                    <Td>{data.id}</Td>
                                    <Td className='title'>{data.autor}</Td>
                                    <Td className='img'><Img src={data.img} /> </Td>
                                    <Td className='des'>{data.description}</Td>
                                    <Td>{data.link}</Td>
                                </Tr>
                            ))
                        }
                    </TBody>
                </Table>
            </Container>           
        </>
    )
}

export default Creditos
