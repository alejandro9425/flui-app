import React from 'react'
import moment from 'moment'
import 'moment/locale/es'
import {Tr,Td, Image} from '../../../elements/ElementsListDocentes';
import { BotonDangerState, BotonSuccessState } from '../../../elements/ElementsBotones'
import { useDispatch } from 'react-redux';
import { startUpdateState } from '../../../actions/docentes';
const Docente = ({id,photoURL,displayName, email, date, state}) => {
    const dispatch = useDispatch();
    const handleUpdateState = (id,email,state) => {
        dispatch(startUpdateState(id, email, state));
    }
    return (
        <Tr>
           <Td><Image src={photoURL} /></Td>
           <Td>{displayName}</Td>
           <Td>{email}</Td>
           <Td>{moment(date).fromNow()}</Td>
           <Td>{state === true ? 'Activo' : 'Inactivo'}</Td>
           <Td>
               {
                   state === true ?
                   (
                    <BotonDangerState onClick={() =>handleUpdateState(id,email, state)}>Desacticar</BotonDangerState>
                   )
                   :
                   (
                    <BotonSuccessState onClick={() =>handleUpdateState(id, email, state)}>Activar</BotonSuccessState>
                   )
               }
            
            </Td> 
        </Tr>
    )
}

export default Docente
