import React from 'react'
import Header from '../layouts/Header';
import {H1, ContainerDocente, Table, 
    THead, TBody, Tr, Th} from '../../../elements/ElementsListDocentes';
import Docente from './Docente';
import { useSelector } from 'react-redux';
const Docentes = () => {
    const {docentes} = useSelector(state => state.docentes)
    return (
        <>
            <Header />
            <H1>Docentes</H1>
            <ContainerDocente>
                <Table>
                    <THead>
                        <Tr>
                            <Th>Foto Perfil</Th>
                            <Th>Nombre</Th>
                            <Th>Correo</Th>
                            <Th>Fecha</Th>
                            <Th>Estado</Th>
                            <Th>Accion</Th>
                        </Tr>
                    </THead>
                    <TBody>
                        {
                            docentes.map((docente) => (
                                <Docente 
                                    key={docente.id}
                                    {...docente}
                                />
                            ))
                        }
                    </TBody>
                </Table>
            </ContainerDocente>
        </>
    )
}

export default Docentes
