import { db } from '../firebase/firebase';
import { updateNumberTemas } from './asignaturas';

export const CantComments = async (email,idAsignatura,question) => {
    let cantidad = 0;
    await db.collection('Aula').doc(email)
    .collection('Subjects').doc(idAsignatura)
    .collection('comments').where("question","==", question).get()
    .then(query => {
        query.forEach(() => {
            cantidad++;
        })
    })
    return cantidad
}

export const comments = async (email,idAsig,question) => {
    const comentarios = []
    await db.collection('Aula').doc(email)
    .collection('Subjects').doc(idAsig)
    .collection('comments').where("question","==", question).get()
    .then(query => {
        query.forEach(data => {
            comentarios.push({
                id: data.id,
                ...data.data()
            })
        })
    })
    return comentarios
}


export const getTemaId = async (id, data = []) => {
    return data.find(asig => asig.id === id)
}

export const delQuestion = async (email, idAsig,idTema,idQuestion) => {
    return db.collection('Aula').doc(email)
    .collection('Subjects').doc(idAsig).collection('topics')
    .doc(idTema).collection('Seccion5').doc(idQuestion).delete()
}

export const delTopics = async (email, idAsig,idTema) => {
    await db.collection('Aula').doc(email)
    .collection('Subjects').doc(idAsig).collection('topics')
    .doc(idTema).delete().then(() =>{
        db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsig).get()
        .then(response => {
            updateNumberTemas(email,idAsig,response.data().themes-1)
        })
    });
}

export const cantThemes = async (email,idAsig) => {
    const temas = []
    await db.collection('Aula').doc(email)
    .collection('Subjects').doc(idAsig).onSnapshot(query => {
        temas.push({...query.data()})
    });
    return temas
}


export const getStudents = async (email, idAsig) => {
    const estudiantes = []
    await db.collection('Aula').doc(email).collection('Subjects')
    .doc(idAsig).collection('enrolled')
    .orderBy('date','desc')
    .where('domain', '==', '@estudiantesunibague.edu.co')
    .get()
    .then(query => {
        query.forEach(data => {
            estudiantes.push({
                id: data.id,
                ...data.data()
            })
        })
    });
    return estudiantes
}

export const getProgressStudent = async (email,idAsig) => {
    const progress = []
    await db.collection('Aula').doc(email).collection('Subjects')
    .doc(idAsig).collection('progress')
    .where('domain','==', '@estudiantesunibague.edu.co')
    .orderBy('date','desc')
    .get().then(query => {
        query.forEach(data => {
            progress.push({
                id: data.id,
                ...data.data()
            })
        })
    })
    return progress;
}

export const getCurso = async (email, idAsig) => {
    const asig = []
    await db.collection('Aula').doc(email).collection('Subjects')
    .doc(idAsig).get().then(query => {
        asig.push({...query.data()})
    })
    return asig
}



export const Subjects = async (email) => {
    const nameAsig = []
    const students = []
    const likes = []
    await db.collection('Aula').doc(email).collection('Subjects')
    .get().then(query => {
        query.forEach(data => {
            nameAsig.push(data.data().nameSubject);
            students.push(data.data().students);
            likes.push(data.data().likes);
        })
    })
    return {nameAsig, students,likes}
}