import { db, storage } from '../firebase/firebase'
import { urlImgAsig } from '../interfaces/Collections';
let collection = 'Aula';
let collection2 = 'Subjects'

export const createAsignatura = async (email, nombreAsig) => {
    return await db.collection(collection).doc(email)
        .collection(collection2).add({
            comments: 0,
            likes: 0,
            nameSubject: nombreAsig,
            notlikes: 0,
            photoURL: urlImgAsig.url,
            students: 0,
            themes: 0
        })
}

export const getAsignaturas = async (email) => {
    const asignaturas = [];
    const dataAsignaturas = await db.collection(collection).doc(email).collection(collection2).get();
    dataAsignaturas.forEach(queryAsignaturas => {
        asignaturas.push({
            id: queryAsignaturas.id,
            ...queryAsignaturas.data()
        })
    })
    return asignaturas;
}

export const updateNameAsignatura = async (email, id, newName) => {
    const response = await db.collection(collection).doc(email).collection(collection2).doc(id).update({
        nameSubject: newName
    });
    return response;
}


export const getAsignaturaId = async (id, data = []) => {
    return data.find(asig => asig.id === id)
}


export const updateNumberTemas = async (email, id, themes) => {
    const response = await db.collection(collection).doc(email)
        .collection(collection2).doc(id).update({
            themes: themes
        });

    return response;
}

export const seleccionarPhotoAsignatura = async (image, id, email, nameAsignatura) => {
    if (image.target.files[0].type === undefined) {
        console.log("Error")
        return
    }

    if (image.target.files[0].type === "image/jpeg" || image.target.files[0].type === 'image/png') {
        try {

            const imagenRef = await storage.ref().child('Foto Asignatura').child(email).child(nameAsignatura)
            await imagenRef.put(image.target.files[0])
            const imagenURL = await imagenRef.getDownloadURL()
            await db.collection(collection).doc(email)
                .collection(collection2).doc(id).update({
                    photoURL: imagenURL
                })
        } catch (error) {
            console.log(error);
        }
    }
}