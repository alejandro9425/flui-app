import { db } from '../firebase/firebase';

export const loadDocentes = async () => {
    const docentes = [];
    const docentelist = await db.collection('usuariosW')
    .where("rol","==","docente")
    .get();
    docentelist.forEach(snapDocente => {
        docentes.push({
            id: snapDocente.id,
            ...snapDocente.data()
        })
    })
    return docentes;
}