import { asignaturasLogout, estudianteslogout } from "../actions/asignaturas";
import { login, logout } from "../actions/auth";
import { introduccionLogout, seccion1Logout, seccion2Logout, seccion3Logout, seccion4Logout, seccion5Logout } from "../actions/contenido";
import { docenteLogout } from "../actions/docentes";
import { fraseLogout } from "../actions/frase";
import { perfilLogout } from "../actions/perfil";
import { temasLogout } from "../actions/temas";
import { finishLoading, startLoading } from "../actions/ui";
import { auth, db } from "../firebase/firebase"

export const startRegisterWithEmailPasswordName = (email, password, name) => {
    return (dispatch) => {
        auth.createUserWithEmailAndPassword(email, password)
            .then(async ({ user }) => {
                await user.updateProfile({ displayName: name });
                await db.collection('usuariosW').doc(user.email).set({
                    date: Date.now(),
                    description: 'Aun no he hecho mi descripción',
                    displayName: name,
                    email: user.email,
                    faculty: '',
                    photoURL: 'https://w7.pngwing.com/pngs/81/570/png-transparent-profile-logo-computer-icons-user-user-blue-heroes-logo-thumbnail.png',
                    program: '',
                    rol: 'docente',
                    state: false,
                    subjects: 'Aun no he agregado mis asignaturas',
                    uid: user.uid,
                    gender: ''
                })
                dispatch(login(user.uid,user.displayName))
            }).catch(e => {
                console.log(e);
            })
    }
}

// export const startLoginEmailPassword = (email, password) => {
//     return (dispatch) => {
//         dispatch(startLoading());
//         auth.signInWithEmailAndPassword(email,password)
//         .then(({user}) => {
//             dispatch(login(user.uid, user.displayName));
//             dispatch(finishLoading());
//         }).catch(e => {
//             dispatch(finishLoading());
//             console.log(e);
//         })
//     }
// }

export const startLoginEmailAndPassword = (email, password) => {
    return auth.signInWithEmailAndPassword(email,password)
}

export const startLoginEmailPassword = (user) => {
    return (dispatch) => {
        dispatch(startLoading());
        dispatch(login(user.uid, user.displayName));
        dispatch(finishLoading());
    }
}

export const startLogout = () => {
    return async (dispatch) => {
        await auth.signOut();
        dispatch(logout());
        dispatch(perfilLogout());
        dispatch(docenteLogout());
        dispatch(fraseLogout());
        dispatch(asignaturasLogout());
        dispatch(temasLogout());
        dispatch(introduccionLogout());
        dispatch(seccion1Logout());
        dispatch(seccion2Logout());
        dispatch(seccion3Logout());
        dispatch(seccion4Logout());
        dispatch(seccion5Logout());
        dispatch(estudianteslogout());
    }
} 