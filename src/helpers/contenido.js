import { db, storage } from '../firebase/firebase';

export const getIntroduccion = async (email, idAsignatura, idTema) => {
    const data = [];
    await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema).collection('Introduction').get()
        .then((query) => {
            query.forEach((doc) => {
                data.push({
                    id: doc.id,
                    ...doc.data()
                });
            });

        })
    return data
}

export const updateIntroduccion = async (email, idAsignatura, idTema, idSeccion, descripcion) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection('Introduction').doc(idSeccion).update({
            description: descripcion
        })
}

export const seleccionarPhoto = async (image, email, idAsignatura, idTema, idSeccion) => {
    if (image.target.files[0].type === undefined) {
        console.log("Error")
        return
    }
    if (image.target.files[0].type === "image/jpeg" || image.target.files[0].type === 'image/png') {
        try {

            const imagenRef = await storage.ref().child('Aula').child('Curso').child(email)
                .child(idAsignatura).child(idTema).child(idSeccion)
            await imagenRef.put(image.target.files[0])
            const imagenURL = await imagenRef.getDownloadURL()

            await db.collection('Aula').doc(email)
                .collection('Subjects').doc(idAsignatura)
                .collection('topics').doc(idTema)
                .collection('Introduction').doc(idSeccion).update({
                    picture: imagenURL
                })
        } catch (error) {
            console.log(error);
        }
    }
}


export const getSeccion1 = async (email, idAsignatura, idTema) => {
    const data = [];
    await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema).collection('Seccion1').get()
        .then((query) => {
            query.forEach((doc) => {
                data.push({
                    id: doc.id,
                    ...doc.data()
                });
            });

        })
    return data
}

export const getSeccion2 = async (email, idAsignatura, idTema) => {
    const data = [];
    await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema).collection('Seccion2').get()
        .then((query) => {
            query.forEach((doc) => {
                data.push({
                    id: doc.id,
                    ...doc.data()
                });
            });
        })

    return data
}

export const getSeccion3 = async (email, idAsignatura, idTema) => {
    const data = [];
    await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema).collection('Seccion3').get()
        .then((query) => {
            query.forEach((doc) => {
                data.push({
                    id: doc.id,
                    ...doc.data()
                });
            });
        })

    return data
}

export const getSeccion4 = async (email, idAsignatura, idTema) => {
    const data = [];
    await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema).collection('Seccion4').get()
        .then((query) => {
            query.forEach((doc) => {
                data.push({
                    id: doc.id,
                    ...doc.data()
                });
            });
        })

    return data
}

export const getSeccion5 = async (email, idAsignatura, idTema) => {
    const data = [];
    const response = await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema).collection('Seccion5').get();
    response.forEach(query => {
        data.push({
            id: query.id,
            ...query.data()
        })
    })

    return data;
}

export const setSeccion1Video = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, video) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection(seccionColeccion).doc(idSeccion).update({
            content: true,
            video: video
        })
}

export const setSeccion1Description = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, description) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection(seccionColeccion).doc(idSeccion).update({
            content: true,
            description: description
        })
}

export const setSeccionLink = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, link) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection(seccionColeccion).doc(idSeccion).update({
            content: true,
            link: link
        })
}

export const setSeccionPicture = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, picture) => {
    if (picture.target.files[0].type === undefined) {
        console.log("Error");
        return
    }

    if (picture.target.files[0].type === "image/jpeg" || picture.target.files[0].type === 'image/png') {
        try {
            const imagenRef = await storage.ref().child('Aula').child('Curso').child(email).child(idAsignatura)
            .child(idSeccion)
            await imagenRef.put(picture.target.files[0])
            const imagenURL = await imagenRef.getDownloadURL()
            await db.collection('Aula').doc(email)
                .collection('Subjects').doc(idAsignatura)
                .collection('topics').doc(idTema)
                .collection(seccionColeccion).doc(idSeccion).update({
                    content: true,
                    picture: imagenURL
                })
        } catch (error) {
            console.log(error);
        }
    }

}


export const setSeccionDescripcion = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, title) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection(seccionColeccion).doc(idSeccion).update({
            content: true,
            title: title
        })
}



export const setTitleEnlace = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, title) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection(seccionColeccion).doc(idSeccion).update({
            content:true,
            title: title
        })
}

export const setEnlace1 = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, nameLink, link) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection(seccionColeccion).doc(idSeccion).update({
            content:true,
            namelink1: nameLink,
            link1: link
        })
}

export const setEnlace2 = async (email, idAsignatura, idTema, seccionColeccion, idSeccion, nameLink, link) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('topics').doc(idTema)
        .collection(seccionColeccion).doc(idSeccion).update({
            content:true,
            namelink2: nameLink,
            link2: link
        })
}


export const updateNotaQuestion = async (email, idAsignatura, idComentario, nota) => {
    return await db.collection('Aula').doc(email)
        .collection('Subjects').doc(idAsignatura)
        .collection('comments').doc(idComentario)
        .update({
            qualification: parseFloat(nota)
        })
}