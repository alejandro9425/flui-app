import { db } from '../firebase/firebase'
import {Introduction} from '../interfaces/Introduction'
import {Seccion} from '../interfaces/Seccion'
import {Seccion4} from '../interfaces/SeccionLinks'
import {Seccion5} from '../interfaces/SeccionQuestions'
let collection = 'Aula'
let collection2 = 'Subjects'
let temascollection = 'topics'

export const getTemas = async (email, IdAsig) => {
    const temas = [];
    const dataTemas = await db.collection(collection).doc(email)
    .collection(collection2).doc(IdAsig).collection(temascollection).orderBy('date', 'asc').get();
    dataTemas.forEach(queryTemas => {
        temas.push({
                id: queryTemas.id,
                ...queryTemas.data()
            })
    }) 
    return temas;  
}

export const getTemaById = async (email, IdAsig, temaId) => {
    const tema = await db.collection(collection).doc(email)
    .collection(collection2).doc(IdAsig).collection(temascollection).doc(temaId).get()
    return await tema.data();
}

export const createTema = async (email, IdAsig, nombreTema, fecha) => {
    return await db.collection(collection).doc(email).collection(collection2)
    .doc(IdAsig).collection(temascollection).add({name: nombreTema,date:fecha})
    .then((response) => {
       db.collection(collection).doc(email)
       .collection(collection2).doc(IdAsig).collection(temascollection)
       .doc(response.id).collection('Seccion1','Seccion2','Seccion3').add(Seccion);

       db.collection(collection).doc(email)
       .collection(collection2).doc(IdAsig).collection(temascollection)
       .doc(response.id).collection('Seccion2').add(Seccion);

       db.collection(collection).doc(email)
       .collection(collection2).doc(IdAsig).collection(temascollection)
       .doc(response.id).collection('Seccion3').add(Seccion);

       db.collection(collection).doc(email)
       .collection(collection2).doc(IdAsig).collection(temascollection)
       .doc(response.id).collection('Seccion4').add(Seccion4);

       db.collection(collection).doc(email)
       .collection(collection2).doc(IdAsig).collection(temascollection)
       .doc(response.id).collection('Seccion5').add(Seccion5);

       db.collection(collection).doc(email)
       .collection(collection2).doc(IdAsig).collection(temascollection)
       .doc(response.id).collection('Introduction').add(Introduction);

    })
}

export const addQuestion = async (email,IdAsig,idTema, newQuestion) => {
    return await db.collection(collection).doc(email)
    .collection(collection2).doc(IdAsig).collection(temascollection)
    .doc(idTema).collection('Seccion5').add({
        question: newQuestion
    })
}

export const updateQuestion = async (email,IdAsig, idTema,idQuestion, newquestion) => {
    return await db.collection(collection).doc(email)
    .collection(collection2).doc(IdAsig).collection(temascollection)
    .doc(idTema).collection('Seccion5').doc(idQuestion).update({
        question: newquestion
    })
}


export const updateTopicName = async (email,idAsig, idTema,name) => {
    return await db.collection('Aula')
    .doc(email).collection('Subjects').doc(idAsig).collection('topics')
    .doc(idTema).update({
        name:name
    })
}