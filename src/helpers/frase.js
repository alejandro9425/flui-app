import { db } from '../firebase/firebase'

export const loadFrase = async (email) => {
    const frase = [];
    const fraseSnap = await db.collection('fraseDelDia').doc(email).get();
    frase.push({
        ...fraseSnap.data()
    })
    return frase;
}