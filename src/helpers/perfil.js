import { db, storage } from '../firebase/firebase'

export const loadPerfil = async (uid) => {
    const perfilSnap = await db.collection('usuariosW').doc(uid).get();
    const perfil = [];
        perfil.push({
            id: perfilSnap.data().uid,
            ...perfilSnap.data()
        });
    return perfil;
}

export const seleccionarPhoto = async (image, email) => {
    if(image.target.files[0].type === undefined){
        console.log("Error")
        return
    }
    if(image.target.files[0].type === "image/jpeg" || image.target.files[0].type === 'image/png'){
        try {
            const imagenRef = await storage.ref().child('Foto Perfil').child(email).child('foto perfil')
            await imagenRef.put(image.target.files[0])
            const imagenURL = await imagenRef.getDownloadURL()
            await db.collection('usuariosW').doc(email).update({
                photoURL: imagenURL
            })
        } catch (error) {
            console.log(error);
        }
    }
}

export const setDescription = async (description, email) => {
    return await db.collection('usuariosW').doc(email).update({
        description: description
    });   
}

export const setDisplayName = async (displayName, email) => {
    return await db.collection('usuariosW').doc(email).update({
        displayName: displayName
    });   
}

export const setAsignaturas = async (asignaturas, email) => {
    return await db.collection('usuariosW').doc(email).update({
        subjects: asignaturas
    });   
}

export const setFacultad = async (facultad, email) => {
    return await db.collection('usuariosW').doc(email).update({
        faculty: facultad
    });   
}

export const setPrograma = async (programa, email) => {
    return await db.collection('usuariosW').doc(email).update({
        program: programa
    });   
}

export const setGender = async (genero, email) => {
    return await db.collection('usuariosW').doc(email).update({
        gender: genero
    });   
}