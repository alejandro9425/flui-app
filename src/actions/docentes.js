import { db } from '../firebase/firebase';
import { loadDocentes } from '../helpers/docentes'
import { types } from '../types/types';

export const startLoadingdocentes = () => {
    return async (dispatch) => {
        const docentes = await loadDocentes();
        dispatch(setDocentes(docentes))
    }
}
const setDocentes = (docentes) => ({
    type: types.docentesLoad,
    payload: docentes
})


export const startUpdateState = (id, email, state) => {
    return async (dispatch) => {
        await db.collection('usuariosW').doc(email).update({
            state: !state
        });
        const docentes = await loadDocentes();
        dispatch(setDocentes(docentes))
    }
}



export const docenteLogout = () => ({
    type: types.docentesLogoutClieaning,
})