import { types } from '../types/types'
import { getIntroduccion, getSeccion1, getSeccion2, getSeccion3, getSeccion4, getSeccion5 } from "../helpers/contenido"


export const setIntroduccion = (email,idAsignatura,idTema) => {
    return async (dispatch) => {
        const seccion = await getIntroduccion(email,idAsignatura,idTema);
            if (seccion.length > 0) {
                const  {description, contents,  picture, id} = seccion[0]
                
                dispatch(setcontenidoIntroduccion(id, description,contents, picture))
            }   
    }
}
const setcontenidoIntroduccion =  (id, descripcion,contenido, imagen) => ({
    type: types.loadIntroduccion,
    payload: {id, descripcion,contenido, imagen}
})

export const setIntroDescription = (description) => ({
    type: types.introduccionSetDescripcion,
    payload:{
        description
    }
})

export const setIntroImagen = (imagen) => ({
    type: types.introduccionSetImagen,
    payload:{
        imagen
    }
})

export const setSeccion1 = (email,idAsignatura,idTema) => {
    return async (dispatch) => {
        const seccion = await getSeccion1(email,idAsignatura,idTema);
        if (seccion.length > 0) {
            const  {title,description,picture,video,link, contents, id} = seccion[0]
            dispatch(setcontenidoSeccion1(title,description,picture,video,link, contents,id));
        }
    }    
}
const setcontenidoSeccion1 =  (titulo,descripcion,imagen,video,enlace,contenido, id) => ({
    type: types.loadSeccion1,
    payload: {titulo,descripcion,imagen,video,enlace, contenido, id}
})

export const setSeccion2 = (email,idAsignatura,idTema) => {
    return async (dispatch) => {
        const seccion = await getSeccion2(email,idAsignatura,idTema);
        if (seccion.length > 0) {
            const  {title,description,picture,video,link, contents, id} = seccion[0]
            dispatch(setcontenidoSeccion2(title,description,picture,video,link, contents, id))
        }
    }
}
const setcontenidoSeccion2 =  (titulo,descripcion,imagen,video,enlace, contenido, id) => ({
    type: types.loadSeccion2,
    payload: {titulo,descripcion,imagen,video,enlace, contenido, id}
})

export const setSeccion3 = (email,idAsignatura,idTema) => {
    return async (dispatch) => {
        const seccion = await getSeccion3(email,idAsignatura,idTema);
        if (seccion.length > 0) {
            const  {title,description,picture,video,link, contents, id} = seccion[0]
            dispatch(setcontenidoSeccion3(title,description,picture,video,link, contents, id))
        }
    }
}
const setcontenidoSeccion3 =  (titulo,descripcion,imagen,video,enlace, contenido, id) => ({
    type: types.loadSeccion3,
    payload: {titulo,descripcion,imagen,video,enlace, contenido, id}
})

export const setSeccion4 = (email,idAsignatura,idTema) => {
    return async (dispatch) => {
        const seccion = await getSeccion4(email,idAsignatura,idTema);
        if (seccion.length > 0) {
            const  {id, title,namelink1,link1,namelink2,link2, contents} = seccion[0]
            dispatch(setcontenidoSeccion4(id,title,namelink1,link1,namelink2,link2, contents))
        }
    }
}
const setcontenidoSeccion4 =  (id, titulo,nombrelink1,link1,nombrelink2,link2, contenido) => ({
    type: types.loadSeccion4,
    payload: {id, titulo,nombrelink1,link1,nombrelink2,link2, contenido}
})

export const setSeccion5 = (email,idAsignatura,idTema) => {
    return async (dispatch) => {
        const seccion = await getSeccion5(email,idAsignatura,idTema);
        dispatch(setcontenidoSeccion5(seccion))
        
    }
}
const setcontenidoSeccion5 =  (seccion) => ({
    type: types.loadSeccion5,
    payload: seccion
})

/////////////////
export const setseccion1linkVideo = (videolink) => ({
    type: types.uploadSeccion1Video,
    payload: {videolink}
})
export const setseccion2linkVideo = (videolink) => ({
    type: types.uploadSeccion2Video,
    payload: {videolink}
})
export const setseccion3linkVideo = (videolink) => ({
    type: types.uploadSeccion3Video,
    payload: {videolink}
})

/////////////////
export const setseccion1descripcion = (descripcion) => ({
    type: types.uploadSeccion1description,
    payload: {descripcion}
})

export const setseccion2descripcion = (descripcion) => ({
    type: types.uploadSeccion2description,
    payload: {descripcion}
})

export const setseccion3descripcion = (descripcion) => ({
    type: types.uploadSeccion3description,
    payload: {descripcion}
})
/////////////////
export const setseccion1link = (enlace) => ({
    type: types.uploadSeccion1link,
    payload: {enlace}
})
export const setseccion2link = (enlace) => ({
    type: types.uploadSeccion2link,
    payload: {enlace}
})
export const setseccion3link = (enlace) => ({
    type: types.uploadSeccion3link,
    payload: {enlace}
})

/////////////////
export const setseccion1imagen = (imagen) => ({
    type: types.uploadSeccion1Picture,
    payload: {imagen}
})

export const setseccion2imagen = (imagen) => ({
    type: types.uploadSeccion2Picture,
    payload: {imagen}
})

export const setseccion3imagen = (imagen) => ({
    type: types.uploadSeccion3Picture,
    payload: {imagen}
})

/////////////////

export const setseccion1Titulo = (titulo) => ({
    type: types.uploadSeccion1Titulo,
    payload: {titulo}
})

export const setseccion2Titulo = (titulo) => ({
    type: types.uploadSeccion2Titulo,
    payload: {titulo}
})

export const setseccion3Titulo = (titulo) => ({
    type: types.uploadSeccion3Titulo,
    payload: {titulo}
})


export const setseccionQuestion = (id, question) => ({
    type: types.uploadQuestion,
    payload: {id, question}
})


export const refreshNewPregunta = (id, question) => ({
    type: types.refreshQuestion,
    payload: {
        id,question
    }
})

export const refreshTitulo = (titulo) => ({
    type: types.refreshLinksTitle,
    payload: {
        titulo
    }
})

export const refreshNameLink1 = (nombrelink, link) => ({
    type: types.refreshNameLink1,
    payload: {
        nombrelink,link
    }
})

export const refreshNameLink2 = (nombrelink, link) => ({
    type: types.refreshNameLink2,
    payload: {
        nombrelink,link
    }
})



export const deleteQuestionAction = (id) => ({
    type: types.delQuestion,
    payload: id
})


export const introduccionLogout = () => ({
    type: types.logoutIntroduccion,
})

export const seccion1Logout = () => ({
    type: types.logoutSeccion1,
})

export const seccion2Logout = () => ({
    type: types.logoutSeccion2,
})
export const seccion3Logout = () => ({
    type: types.logoutSeccion3,
})
export const seccion4Logout = () => ({
    type: types.logoutSeccion4,
})
export const seccion5Logout = () => ({
    type: types.logoutSeccion5,
})