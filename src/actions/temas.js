import {getTemas} from '../helpers/temas';
import { types } from '../types/types';

export const  startLoadTemas = (email, id) => {
    return async (dispatch) => {
        const temas = await getTemas(email, id);
        dispatch(setTemas(temas))
    }
}
const setTemas = (temas) => ({
    type: types.asignaturaLoadTema,
    payload: temas
})

export const startNewTema = (id, nameTema, fecha) => {
    return async (dispatch) => {
        const newTema = {
            name: nameTema,
            date: fecha
        }
        dispatch(addNewTema(id, newTema));
    }
}

const addNewTema = (id, data) => ({
    type: types.asignaturaAddTema,
    payload: {
        id, ...data
    }
})


export const refreshthemes = (id,nameSubject,themes,photoURL,students,comments,likes) => ({
    type: types.refreshCantidadThemas,
    payload: {
        id,
        nameSubject,
        themes,
        photoURL,
        students,
        comments,
        likes
    }
})

export const deleteThemes = (id) => ({
    type: types.temaDel,
    payload: id
})

export const refreshTemas = (id,date,name) => ({
    type: types.temaUpdate,
    payload: {
        id,
        date,
        name
    }
})

export const temasLogout = () => ({
    type: types.temasLogoutClieaning,
})