import {getAsignaturas, updateNameAsignatura} from '../helpers/asignaturas'
import {types} from '../types/types'

export const startLoadingAsignatura = (email) => {
    return async (dispatch) => {
        const asignaturas = await getAsignaturas(email);
        dispatch(setAsignaturas(asignaturas))
    }
}
const setAsignaturas = (asignaturas) => ({
    type: types.asignaturaLoad,
    payload: asignaturas
})

export const startUpdateNameAsignatura = (email, uid, asignaturaname,comments,students,photoURL, likes,themes) =>{
    return async (dispatch) => {
        updateNameAsignatura(email,uid, asignaturaname).then(() => {
            dispatch(refreshAsignatura(uid,asignaturaname,comments,students,photoURL, likes,themes));
        })
        
    }
}

export const refreshImagenAsignatura = (id, nameSubject,comments,students,photoURL, likes,themes) => ({
    type: types.refreshImagenAsig,
    payload: {
        id,
        nameSubject,
        comments,
        students,
        photoURL,
        likes,
        themes
    }
})

export const refreshAsignatura = (id, nameSubject,comments,students,photoURL, likes,themes) => ({
    type: types.asignaturaUpdate,
    payload: {
        id,
        nameSubject,
        comments,
        students,
        photoURL,
        likes,
        themes
    }
})

export const startNewAsignatura = (id, nameAsig,url) => {
    return async (dispatch) => {
        const newAsi = {
            comments: 0,
            likes: 0,
            nameSubject: nameAsig,
            photoURL: url,
            students: 0,
            themes: 0
        }
        dispatch(addNewAsig(id, newAsi));
    }
}

const addNewAsig = (id, newAsi) => ({
    type: types.asignaturaAdd,
    payload: {
        id, ...newAsi
    }
})

export const asignaturasLogout = () => ({
    type: types.asignaturaLogoutClieaning,
})

export const estudiantesLoad = (estudiantes) => ({
    type: types.studentsLoad,
    payload: estudiantes
})

export const estudianteslogout = () => ({
    type: types.studentsLogoutClieaning
})

