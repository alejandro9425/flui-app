import { loadPerfil } from '../helpers/perfil';
import {types} from '../types/types'

export const startLoadingPerfil = (puid) => {
    return async (dispatch) => {
        const perfil = await loadPerfil(puid);
        const {uid, email, rol, state, displayName, photoURL, faculty,program, subjects,description, gender} = perfil[0];

        dispatch(setPerfil(uid, email, rol, state, displayName, photoURL, faculty,program, subjects,description, gender))
    }
}

export const updatePhotoPerfil = (url) => ({
    type: types.perfilPhotoUpdate,
    payload: {
        url
    }
})

const setPerfil = (uid, email, rol, state, displayName, photoURL, faculty,program, subjects,description,gender) => ({
    type: types.perfilLoad,
    payload: {
        uid, email, rol, state, displayName, photoURL, faculty, program, subjects,description,gender
    }
})

export const setPerfilDescription = (description) => ({
    type: types.perfildescription,
    payload:{
        description
    }
})

export const setPerfilDisplayName = (displayName) => ({
    type: types.perfilDisplayName,
    payload:{
        displayName
    }
});

export const setPerfilFacultad = (faculty) => ({
    type: types.perfilFacultadUpdate,
    payload:{
        faculty
    }
})

export const setPerfilPrograma = (program) => ({
    type: types.perfilProgramaUpdate,
    payload:{
        program
    }
})

export const setPerfilGender = (gender) => ({
    type: types.perfilGenderUpdate,
    payload:{
        gender
    }
})



export const setPerfilAsignaturas = (subjects) => ({
    type: types.perfilAsignatura,
    payload:{
        subjects
    }
});

export const perfilLogout = () => ({
    type: types.perfilLogoutClieaning,
})