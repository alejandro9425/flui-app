import { db } from "../firebase/firebase";
import { loadFrase } from "../helpers/frase";
import { types } from "../types/types";

export const startLoadingFrase = (puid) => {
    return async (dispatch) => {
        const dataFrase = await loadFrase(puid);
        const {sentence, uid} = dataFrase[0];

        dispatch(setFrase(uid,sentence))
    }
}

const setFrase = (uid, sentence) => ({
    type: types.fraseLoad,
    payload: {
        uid,
        sentence
    }
})


export const startUpdateFrase = (email, uid, sentence) => {
    return async (dispatch) => {
        
        await db.collection('fraseDelDia').doc(email).set({
            sentence: sentence,
            uid: email
        })

        dispatch(refreshFrase(sentence, uid))

    }
}

export const refreshFrase = (sentence, uid) => ({
    type: types.fraseUpdate,
    payload: {
        uid,
        sentence
    }
})

export const fraseLogout = () => ({
    type: types.fraseLogoutClieaning,
})