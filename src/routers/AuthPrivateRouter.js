import React from 'react'
import { useSelector } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import CreateAsignaturas from '../components/dashboard/docentes/CreateAsignaturas'
import AsignaturaId from '../components/dashboard/docentes/AsignaturaId'
import AsignaturaTema from '../components/dashboard/docentes/AsignaturaTema'
import FraseDelDia from '../components/dashboard/docentes/FraseDelDia'
import Perfil from '../components/dashboard/docentes/Perfil'
import Home from '../components/dashboard/Home'
import Comentarios from '../components/dashboard/docentes/components/Comentarios'
import ListEstudiantes from '../components/dashboard/docentes/ListEstudiantes'
import Progress from '../components/dashboard/docentes/Progress'
import Contribuciones from '../components/dashboard/Contribuciones'
import Estadisticas from '../components/dashboard/docentes/Estadisticas'
import Docentes from '../components/dashboard/admin/Docentes'
import AcountNotActivated from '../components/dashboard/docentes/AcountNotActivated'
import Creditos from '../components/dashboard/Creditos'
import Flui from '../components/dashboard/Flui'

const AuthPrivateRouter = () => {
    const {rol, state} = useSelector(state => state.perfil)
    return (
        <>
            <Switch>
                {
                    state === false?
                    <Route exact path='/' component={AcountNotActivated} />
                    :
                    <Route exact path='/' component={Home} />
                }
                {
                    rol === 'admin' &&
                    <>
                        <Route exact path='/docentes' component={Docentes} />
                    </>
                }
                {
                    rol === 'docente' &&
                        <>
                            <Route exact path='/home' component={Home} />
                            <Route exact path='/perfil' component={Perfil} />
                            <Route exact path='/Reflexiones' component={FraseDelDia} />
                            <Route exact path='/crear-asignatura' component={CreateAsignaturas} />
                            <Route exact path='/asignatura/:idAsignatura' component={AsignaturaId} />
                            <Route exact path='/asignatura/:idAsignatura/:idTema' component={AsignaturaTema} />
                            <Route exact path='/asignatura/respuestas/:idAsignatura/:idTema/:idQuestion/:idpregunta' component={Comentarios} />
                            <Route exact path='/estudiantes/asignatura/:idAsignatura/:asig' component={ListEstudiantes} />
                            <Route exact path='/progress/asignatura/:idAsignatura' component={Progress} />
                            <Route exact path='/contribuciones' component={Contribuciones} />
                            <Route exact path='/estadisticas' component={Estadisticas} />
                            <Route exact path='/creditos' component={Creditos} />
                            <Route exact path='/flui' component={Flui} />
                        </>
                }
            </Switch>
        </>
    )
}

export default AuthPrivateRouter