import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BrowserRouter as Router, Redirect, Switch } from 'react-router-dom'
import { auth } from '../firebase/firebase'
import AuthPublicRouter from './AuthPublicRouter'
import PrivateRouter from './PrivateRouter'
import PublicRouter from './PublicRouter'
import { login } from '../actions/auth'
import { startLoadingPerfil } from '../actions/perfil'
import { startLoadingdocentes } from '../actions/docentes'
import { startLoadingFrase } from '../actions/frase'
import { startLoadingAsignatura } from '../actions/asignaturas'
import AuthPrivateRouter from './AuthPrivateRouter'
const AppRouter = () => {
    const dispatch = useDispatch();
    const { rol } = useSelector(state => state.perfil)
    const [isAuthenticate, setIsAuthenticate] = useState(false);
    const [checking, setChecking] = useState(true);
    useEffect(() => {
        auth.onAuthStateChanged(async (user) => {
            if (user?.uid) {
                dispatch(login(user.uid, user.displayName));
                setIsAuthenticate(true);
                dispatch(startLoadingPerfil(user.email));
                if (rol === 'admin') {
                    dispatch(startLoadingdocentes());
                }
                if (rol === 'docente') {
                    dispatch(startLoadingFrase(user.email));
                    dispatch(startLoadingAsignatura(user.email));

                }
            } else {
                setIsAuthenticate(false);
            }
            setChecking(false);
        })
    }, [rol, dispatch, setChecking, setIsAuthenticate])

    if (checking) {
        return (
            <h1>Cargando...</h1>
        )
    } else {
        return (
            <Router>
                <Switch>
                    <PublicRouter
                        path='/auth'
                        component={AuthPublicRouter}
                        isAuthenticate={isAuthenticate}
                    />
                    <PrivateRouter
                        path='/'
                        component={AuthPrivateRouter}
                        isAuthenticate={isAuthenticate}
                    />

                    <Redirect to='/auth/login' />
                </Switch>
            </Router>
        )
    }
}

export default AppRouter