export const types = {
    //AUTHENTICATION
    login: '[Auth] Login',
    logout: '[Auth] Logout',

    uiSetError: '[UI] set Error', uiRemoveError: '[UI] Remove Error',
    uiStartLoading: '[UI] Start loading', uiFinishLoading: '[UI] Finish loading',

    //PERFIL USER
    perfilLoad: '[PF] Perfil Load',
    perfilLogoutClieaning: '[PF] Perfil Logout Clieaning',
    perfildescription: '[PD] Perfil Update description',
    perfilDisplayName: '[PDN] Perfil Update DisplayNane',
    perfilAsignatura: '[PA] Perfil Update Asignatura',
    perfilPhotoUpdate: '[PU] Perfil Photo Update',
    perfilFacultadUpdate: '[PU] Perfil Facultad Update',
    perfilProgramaUpdate: '[PU] Perfil Programa Update',
    perfilGenderUpdate: '[PU] Perfil Gender Update',
    //DOCENTES
    docentesLoad: '[DL] Docentes Load',
    docentesLogoutClieaning: '[DLC] Docentes Logout Clieaning',
    docentesUpdated: '[DUP] Docentes Update',    

    //FRASE
    fraseLoad: '[FR] Frase Load',
    fraseUpdate: '[FR] Frase Update',
    fraseLogoutClieaning: '[FR] Frase Logout Clieaning',
    fraseRefresh: '[FR] Frase Refresh',

    //ASIGNATURA
    asignaturaLoad: '[AL] Asignatura Load',
    asignaturaAdd: '[ADD] Asignatura Add',
    asignaturaLoadTema: '[ALT] Asignatura Load Tema',
    asignaturaAddTema: '[ADDT] Asignatura Add Tema',
    asignaturaUpdate: '[AR] Asignatura Update',
    asignaturaLogoutClieaning: '[ALC] Asignatura Logout',

    //ASIGNATURA
    asigLoad:'[AL] Asig Load',
    asigLogoutClieaning: '[ALC] Asig Logout',

    //TEMAS
    temasLogoutClieaning: '[TL] Temas Logout',
    temaIdLoading: '[TIL] Tema Loading Id',
    temaUpdate: '[TU] Tema Update',
    temaDel: '[TD] Tema delete',
    contenidoTemaLoad: '[CT] contenido Tema',

    //SECCION
    loadSeccion1: '[LST] Load Seccion 1',
    loadSeccion2: '[LST] Load Seccion 2',
    loadSeccion3: '[LST] Load Seccion 3',
    loadSeccion4: '[LST] Load Seccion 4',
    loadSeccion5: '[LST] Load Seccion 5',
    loadIntroduccion: '[LI] Load Introduccion',

    logoutSeccion1: '[LST] logout Seccion 1',
    logoutSeccion2: '[LST] logout Seccion 2',
    logoutSeccion3: '[LST] logout Seccion 3',
    logoutSeccion4: '[LST] logout Seccion 4',
    logoutSeccion5: '[LST] logout Seccion 5',
    logoutIntroduccion: '[LI] logout Introduccion',

    introduccionSetDescripcion: '[IS] Introduccion Up descripcion',
    introduccionSetImagen: '[IS] Introduccion Up imagen',


    uploadSeccion1Video: '[USV] upload Seccion 1 Video',
    uploadSeccion1description: '[USD] upload Seccion 1 descripcion',
    uploadSeccion1link: '[USL] upload Seccion 1 Link',
    uploadSeccion1Picture: '[USP] upload Seccion 1 Picture',
    uploadSeccion1Titulo: '[UST] upload Seccion 1 Titulo',

    uploadSeccion2Video: '[USV] upload Seccion 2 Video',
    uploadSeccion2description: '[USD] upload Seccion 2 descripcion',
    uploadSeccion2link: '[USL] upload Seccion 2 Link',
    uploadSeccion2Picture: '[USP] upload Seccion 2 Picture',
    uploadSeccion2Titulo: '[UST] upload Seccion 2 Titulo',

    uploadSeccion3Video: '[USV] upload Seccion 3 Video',
    uploadSeccion3description: '[USD] upload Seccion 3 descripcion',
    uploadSeccion3link: '[USL] upload Seccion 3 Link',
    uploadSeccion3Picture: '[USP] upload Seccion 3 Picture',
    uploadSeccion3Titulo: '[UST] upload Seccion 3 Titulo',

    uploadQuestion: '[UQ] upload Question',


    refreshQuestion: '[RQ] refresh Question',

    refreshLinksTitle: '[RL] refresh Title',
    refreshNameLink1: '[RL] refresh Link1',
    refreshNameLink2: '[RL] refresh Link2',

    refreshCantidadThemas: '[RCT] refresh Cantidad Temas',

    refreshImagenAsig: '[RIA] refresh Imagen Asignatura',

    delQuestion: '[DQ] delete Question',

    refreshNota: '[RN] refresh Nota',


    studentsLoad: '[SL] student Load',
    studentsLogoutClieaning: '[SL] student Logout',
}